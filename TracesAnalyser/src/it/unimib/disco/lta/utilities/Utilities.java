package it.unimib.disco.lta.utilities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Utilities {

	public static final char csvSeparator = '|';
	private static final String fixMethodsFileName = "Fix_Methods.txt";

	public static String writeFile(List<String> toWrite, String outputName){
		try {
			File file = new File(outputName);
			FileOutputStream fos = new FileOutputStream(file);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			for (String s : toWrite) {
				bw.write(s + "\n");
			}
			bw.flush();
			bw.close();
			return file.getAbsolutePath();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<String> loadFile(String fileName) {
		try {
			File file = new File(fileName);
			Scanner sc = new Scanner(file);
			List<String> fileLines = new ArrayList<>();
			while (sc.hasNextLine()) {
				fileLines.add(sc.nextLine());
			}
			sc.close();
			return fileLines;
		} catch (Exception e) {
		}		
		return null;
	}

	public static void runtimeExec(String command){
		runtimeExec(command, false);
	}

	public static void runtimeExec(String command, String outputFile){
		ProcessBuilder builder = new ProcessBuilder(Arrays.asList(command.split(" ")));
		builder.redirectOutput(new File(outputFile));
		try {
			Process p = builder.start();
			p.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
		} 	
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void runtimeExec(String command, boolean printOutput){
		Process process;
		try {
			process = Runtime.getRuntime().exec(command);
			BufferedReader stdInput = new BufferedReader(new
					InputStreamReader(process.getInputStream()));

			BufferedReader stdError = new BufferedReader(new
					InputStreamReader(process.getErrorStream()));
			// read the output from the command
			String s = null;
			if (printOutput){
				while ((s = stdInput.readLine()) != null) {
					System.out.println(s);
				}
				// read any errors from the attempted command
				while ((s = stdError.readLine()) != null) {
					System.out.println(s);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void writeFile(String string, String outputName) {
		writeFile(Arrays.asList(string), outputName);
	}

	public static final List<String> ANDROIDFRAMEWORKIDENTIFIERS = Arrays.asList(
			"android.",
			"dalvik.",
			"org.kxml2",
			"sun.",
			"libcore.",
			"java.",
			"org.apache.",
			"com.google."
			);

	public static final List<String> IGNORELISTIDENTIFIERS = Arrays.asList(
			"XResources",
			"java.",
			"xposed",
			"beforeHookedMethod",
			"afterHookedMethod",
			"it.unimib.disco.lta.tracerenabler",
			"android.dex",
			"dalvik.system.",
			"android.text",
			"android.widget.EditText",
			"android.util.Log",
			"android.graphics",
			"com.android.internal.util.FastPrintWriter",
			"android.view.View",
			"android.os.Trace",
			"libcore.",
			"sun.misc",
			"android.view.Choreographer",
			"com.bumptech.glide",
			"android.widget.TextView",
			"android.view.accessibility.AccessibilityManager",
			"android.os.SystemClock",
			"android.os.Binder.clearCallingIdentity",
			"android.os.Handler.removeCallbacks",
			"com.google",
			"org.apache",
			"java.util",
			"javax.",
			"android.os.Parcel"
			);

	public static boolean isToIgnore(String method){
		return _isInList(method,IGNORELISTIDENTIFIERS);
	}

	public static boolean isFrameworkMethod(String method){
		return _isInList(method,ANDROIDFRAMEWORKIDENTIFIERS);
	}

	private static boolean _isInList(String string, List<String> list){
		for (String identifier : list) {
			if (string.toLowerCase().contains(identifier.toLowerCase())){
				return true;
			}
		}
		return false;
	}

	@Deprecated
	public enum RelevantMethods{
		BUG_RELATED,
		FIX_RELATED
	}
	
	@Deprecated
	public static String getRelevantMethods(String folderPath, RelevantMethods type){
		if (type == RelevantMethods.BUG_RELATED){
			return __getBugRelevantMethods(folderPath);
		} else{
			return __getFixRelevantMethods(folderPath);
		}
	}

	//	@Deprecated
	//	private static String _getRelMethods(String folderPath) {
	//		return __getTracePath(folderPath, "relevantmethods");
	//	}

	//	private static final String bugMethodsFileName = "WD_Bug-Related_Methods";
	//	private static final String bugMethodsFileName = "Diff_Bug-Related_Methods";
	@Deprecated
	private static final String bugMethodsFileName = "Bug-Related_Methods";

	@Deprecated
	private static String __getBugRelevantMethods(String folderPath) {
		return __getTracePath(folderPath, bugMethodsFileName);
	}
	@Deprecated
	private static String __getFixRelevantMethods(String folderPath) {
		return __getTracePath(folderPath, "fix-related_methods");
	}
	
	public static String getFixMethodsPath(String folderPath){
		return __getTracePath(folderPath, fixMethodsFileName);
	}

	public static String getBaselinePath(String folderPath) {
		return __getTracePath(folderPath, "baseline");
	}

	public static String getBUGPath(String folderPath) {
		return __getTracePath(folderPath, "bug");
	}

	public static String getFIXPath(String folderPath) {
		String tmp = __getTracePath(folderPath, "fix");
		if (tmp.contains(".txt"))
			return null;
		return tmp;
	}

	public static String getEvaluationFolderPath(String folderPath){
		return getFolderPath(folderPath, "Evaluation", true);
	}
	
	public static String getFolderPath(String rootFolderPath, String lookFor, boolean create){
		File folderFile = new File(rootFolderPath);

		String[] files = folderFile.list();
		if (files == null )
		{
			System.err.println("Unable to read folder: "  + folderFile.getAbsolutePath());
		}
		String outputFolderPath = null;
		
		for (String childFilePath : files) {
			if (childFilePath.toLowerCase().contains(lookFor)){
				File childFile = new File(folderFile.getAbsolutePath() + File.separator + childFilePath);
				if (childFile.isDirectory()){
					outputFolderPath =  childFile.getAbsolutePath();
				}
			}
		}
		if (outputFolderPath == null){
			String basePath = folderFile.getAbsolutePath();
			basePath = basePath.charAt(basePath.length() -1) == File.separatorChar ? basePath : basePath + File.separator;
			File evaluationFolder = new File(basePath + lookFor + File.separator);
			evaluationFolder.mkdirs();
			outputFolderPath = evaluationFolder.getAbsolutePath();
		}
		return outputFolderPath;
		
	}

	private static String __getTracePath(String folderPath, String lookFor) {

		File folderFile = new File(folderPath);
		// 		System.out.println(folderFile.getAbsolutePath());
		String returnPath = null;
		String[] files = folderFile.list();
		if (files == null )
		{
			System.err.println("Unable to read folder: "  + folderFile.getAbsolutePath());
		}
		for (String childFilePath : files) {
			if (childFilePath.toLowerCase().contains(lookFor.toLowerCase())) {
				File childFile = new File(folderPath + File.separator + childFilePath);
				if (childFile.isDirectory()) {
					returnPath = __getTracePath(childFile.getAbsolutePath(), "filtered");
					if (returnPath == null && childFile.list().length > 0) {
						returnPath = folderPath + File.separator + childFile.list()[0];
					}
					break;
				} else {
					returnPath = childFile.getAbsolutePath();
				}
			}
		}
		return returnPath;
	}

}
