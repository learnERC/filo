package it.unimib.disco.lta.fixLocusCandidatesIdentification.rankingGeneration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import it.unimib.disco.lta.app.Filo;
import it.unimib.disco.lta.fixLocusCandidatesIdentification.callGraphGeneration.CallGraph;

public class RankingGenerator {
	
	private static final Logger log = Logger.getLogger(Filo.class);


	public Ranking generateRanking(CallGraph callGraph, String rankingFolder) {
		Ranking toRet = new Ranking(callGraph);
		toRet.writeToFile(rankingFolder + "Ranking_FULL.csv");
		log.info("File " + rankingFolder + "Ranking_FULL.csv file created");
		toRet.prune(true);
		toRet.writeToFile(rankingFolder + "Ranking.csv");
		log.info("File " + rankingFolder + "Ranking.csv file created");
		return toRet;
	}
	
	public Ranking generateRanking(CallGraph callGraph, List<String> suspiciousMethods, Map<Integer, ArrayList<String>> wMap, List<String> mList, double K1, double K2, double K3) {
		Ranking toRet = new Ranking(callGraph);
		toRet.prune(true);
		toRet.computeRanking(suspiciousMethods, wMap, mList, K1, K2, K3);
		return toRet;
	}

}
