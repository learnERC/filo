package it.unimib.disco.lta.fixLocusCandidatesIdentification.rankingGeneration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import it.unimib.disco.lta.fixLocusCandidatesIdentification.callGraphGeneration.CallGraph;
import it.unimib.disco.lta.fixLocusCandidatesIdentification.callGraphGeneration.CallGraphNode;
import it.unimib.disco.lta.utilities.Utilities;

public class Ranking {
	private Map<Double, String> rankingItems;
	private CallGraph referenceCallGraph;
	private double __lastRankingScore = 0.0;

	public CallGraph getReferenceCallGraph() {
		return referenceCallGraph;
	}

	public Map<Double, String> getRankingItems() {
		return rankingItems;
	}

	public void setRankingItems(Map<Double, String> rankingItems) {
		this.rankingItems = rankingItems;
	}

	public Ranking(CallGraph referenceCallGraph) {
		super();
		this.referenceCallGraph = referenceCallGraph;
		__populateRanking();
	}

	private void __populateRanking() {
		this.rankingItems = new HashMap<Double, String>();
		for (CallGraphNode<String> node : referenceCallGraph.asNodesList()) {
			addRankingItem(node.getData(), node.getNormRelevancy());
		}
	}

	public void updateRelevancyThresholds(double repWeightThreshold, double childrenNumberThreshold,
			double depthThreshold) {
		this.referenceCallGraph.setThresholds(repWeightThreshold, childrenNumberThreshold, depthThreshold);
		__populateRanking();
	}

	private void addRankingItem(String methodName, double score) {
		this.rankingItems.put(score, methodName);
	}

	public void writeToFile(String outputName) {
		List<String> toWriteList = new ArrayList<>();
		if (rankingItems != null && rankingItems.size() > 0) {
			toWriteList.add("Position " + Utilities.csvSeparator + "MethodName" + Utilities.csvSeparator + "Relevancy");
			Double[] keys = rankingItems.keySet().toArray(new Double[rankingItems.keySet().size()]);
			Arrays.sort(keys, Collections.reverseOrder());
			for (int i = 0; i < keys.length; i++) {
				double normPosition = ((double) i + 1) / ((double) keys.length);
				toWriteList.add(normPosition + "" + Utilities.csvSeparator + rankingItems.get(keys[i])
						+ Utilities.csvSeparator + keys[i]);
			}
			Utilities.writeFile(toWriteList, outputName);
		}
		toWriteList.clear();

		String tresholdsPath = outputName.substring(0,outputName.lastIndexOf('.')) + "_Tresholds.csv";
		toWriteList.add("RepWeightThreshold" + Utilities.csvSeparator + "ChildrenNumberThreshold"
				+ Utilities.csvSeparator + "DepthThreshold" + Utilities.csvSeparator + "TotalRankingScore");
		toWriteList.add("" + referenceCallGraph.getRootNode().getRepWeightThreshold() + Utilities.csvSeparator
				+ referenceCallGraph.getRootNode().getChildrenNumberThreshold() + Utilities.csvSeparator
				+ referenceCallGraph.getRootNode().getDepthThreshold() + Utilities.csvSeparator + __lastRankingScore);

		Utilities.writeFile(toWriteList, tresholdsPath);
		toWriteList.clear();
	}

	public void prune(boolean removeAndroidMethods) {
		rankingItems = __removeDuplicates();
		Map<Double, String> newMap = new HashMap<>();
		newMap.putAll(rankingItems);
		for (Entry<Double, String> entry : newMap.entrySet()) {
			for (String androidRef : Utilities.ANDROIDFRAMEWORKIDENTIFIERS) {
				if (entry.getValue().contains(androidRef)) {
					rankingItems.remove(entry.getKey());
				}
			}
		}
	}
	
	private boolean __checkForShortClassName(String methodString){
		if (methodString == null || methodString.length() == 0)
			return true;
		int dotIndex = methodString.lastIndexOf(".");
		String classname = methodString.substring(0,dotIndex);
		classname = classname.substring(classname.lastIndexOf('.')+1);
		if (classname.length() <= 4)
			return true;
		return false;
	}

	private Map<Double, String> __removeDuplicates() {
		Map<Double, String> tmpMap = new HashMap<>();
		for (Entry<Double, String> entry : rankingItems.entrySet()) {
			if(!__checkForShortClassName(entry.getValue())) {
				if (tmpMap.containsValue(entry.getValue())) {
					for (Entry<Double, String> tmpEntry : tmpMap.entrySet()) {
						if (tmpEntry.getValue().equals(entry.getValue())) {
							double newKey = tmpEntry.getKey() + entry.getKey();
							tmpMap.remove(tmpEntry.getKey());
							tmpMap.put(newKey, entry.getValue());
	
							break;
						}
					}
				} else {
					tmpMap.put(entry.getKey(), entry.getValue());
				}
			}
		}
		return tmpMap;
	}

	public double getRankingScore(List<String> fixMethods) {
		double rankingScore = 0.0;
		for (String methodToFind : fixMethods) {
			boolean found = false;

			Double[] keys = rankingItems.keySet().toArray(new Double[rankingItems.keySet().size()]);
			Arrays.sort(keys, Collections.reverseOrder());
			for (int i = 0; i < keys.length; i++) {
				if (rankingItems.get(keys[i]).equalsIgnoreCase(methodToFind)) {
					rankingScore += ((double) i + 1);
					found = true;
					break;
				}
			}
			if (!found) {
				rankingScore++; // since the ranking is normalised to 1, the worst value is 1
				rankingScore += (double) keys.length;

			}
		}
		__lastRankingScore = rankingScore;
		return rankingScore;
	}
	
	public void computeRanking(List<String> suspiciousMethods, Map<Integer, ArrayList<String>> wMap, List<String> mList, double K1, double K2, double K3) {
		if (rankingItems != null && rankingItems.size() > 0) {
			Double[] keys = rankingItems.keySet().toArray(new Double[rankingItems.keySet().size()]);
			Arrays.sort(keys, Collections.reverseOrder());
			int rankingSum = 0;
			int methodsFound = 0;
			String methodsInfo = "";
			for (int i = 0; i < keys.length; i++) {
				if(suspiciousMethods.contains(rankingItems.get(keys[i]))) {
					int position = i+1;
					if(position <= 10) {
						double normPosition = ((double) position) / ((double) keys.length);
						rankingSum += position;
						methodsFound++;
						if(methodsFound > 1)
							methodsInfo += ";";
						methodsInfo += "" + position + "-" + normPosition + "-" + rankingItems.get(keys[i])	+ "-" + keys[i];
						
						mList.add("" + position + Utilities.csvSeparator + normPosition + Utilities.csvSeparator
								+ rankingItems.get(keys[i])	+ Utilities.csvSeparator + keys[i] + Utilities.csvSeparator
								+ K1 + Utilities.csvSeparator + K3 + Utilities.csvSeparator + K2 + Utilities.csvSeparator + __lastRankingScore);
					}
				}
			}
			
			int methodsNotFound = suspiciousMethods.size() - methodsFound;
			rankingSum += methodsNotFound * 11;
			
			if(wMap.get(methodsNotFound) == null)
				wMap.put(methodsNotFound, new ArrayList<>());
			
			double finalScore = 11.0 * ((double) suspiciousMethods.size());
			if(methodsFound > 0)
				finalScore = ((double) rankingSum) / ((double) suspiciousMethods.size());

			String zerosScore = "";
			if(finalScore < 10)
				zerosScore = "000";
			else if(finalScore < 100)
				zerosScore = "00";
			else if(finalScore < 1000)
				zerosScore = "0";

			String zerosRS = "";
			if(rankingSum < 10)
				zerosRS = "000";
			else if(rankingSum < 100)
				zerosRS = "00";
			else if(rankingSum < 1000)
				zerosRS = "0";
			
			wMap.get(methodsNotFound).add("" + zerosScore + finalScore + Utilities.csvSeparator + zerosRS + rankingSum + Utilities.csvSeparator + methodsFound + Utilities.csvSeparator
					+ K1 + Utilities.csvSeparator + K3 + Utilities.csvSeparator + K2 + Utilities.csvSeparator + methodsInfo);
		}
	}

}
