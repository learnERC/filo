package it.unimib.disco.lta.fixLocusCandidatesIdentification.callGraphGeneration;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import it.unimib.disco.lta.anomalyDetection.SuspiciousInvocationBlock;
import it.unimib.disco.lta.fixLocusCandidatesIdentification.callGraphGeneration.CallGraphNode.DOTStyleProperty;
import it.unimib.disco.lta.utilities.Utilities;

public class CallGraph {
	private String dotPath ="";
	private CallGraphNode<String> rootNode, mostChildrenNode, mostRelevantNode;
	private String name;
	private List<SuspiciousInvocationBlock> referenceBlocks;

	public List<SuspiciousInvocationBlock> getReferenceBlocks() {
		return referenceBlocks;
	}
	
	public void setReferenceBlocks(List<SuspiciousInvocationBlock> referenceBlocks) {
		this.referenceBlocks = referenceBlocks;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMaxDepth(){
		double maxDepth = 0.0;
		for (CallGraphNode<String> node : asNodesList()) {
			if (node.getDepth() > maxDepth)
				maxDepth = node.getDepth();
		}
		return maxDepth;
	}

	public CallGraph(String name,CallGraphNode<String> rootNode){
		setName(name);
		setRootNode(rootNode);
	}

	public CallGraph(String name, String root) {
		this(name, new CallGraphNode<String>(root));
	}

	public CallGraphNode<String> getMostChildrenNode() {
		return mostChildrenNode;
	}

	private void setMostChildrenNode(CallGraphNode<String> mostChildrenNode) {
		this.mostChildrenNode = mostChildrenNode;
	}

	public CallGraphNode<String> getMostRelevantNode() {
		return mostRelevantNode;
	}

	private void setMostRelevantNode(CallGraphNode<String> mostRelevantNode) {
		this.mostRelevantNode = mostRelevantNode;
	}

	public CallGraphNode<String> getRootNode() {
		return rootNode;
	}

	public void setRootNode(CallGraphNode<String> rootNode) {
		this.rootNode = rootNode;
	}

	@Override
	public String toString() {
		return rootNode.toString();
	}

	public void toDOT(){
		toDOT(getName() +".dot");
	}

	public List<CallGraphNode<String>> getNodesAboveNormRelevancyTreshold(double treshold){
		List<CallGraphNode<String>> toRet = new ArrayList<>();
		for (CallGraphNode<String> node : asNodesList()) {
			if (node.getNormRelevancy() >= treshold){
				toRet.add(node);
			}
		}
		return toRet;
	}

	public void toDOT(String outputName) {
		if (!outputName.contains(".dot")){
			outputName += ".dot";
		}
		List<String> dotStrings = new ArrayList<>();
		String simpleName = outputName;
		if (simpleName.indexOf(File.separator) != -1){
			simpleName = simpleName.substring(simpleName.lastIndexOf(File.separator)+1);
		}
		dotStrings.add("digraph " + simpleName.substring(0, simpleName.lastIndexOf('.')) + " {");
		dotStrings.addAll(getRootNode().toDOT(UUID.randomUUID().toString()));
		dotStrings.add("}");
		Utilities.writeFile(dotStrings, outputName);
		this.dotPath = outputName;
	}

	public void clearDOTProperties(){
		List<CallGraphNode<String>> nodes = asNodesList();
		for (CallGraphNode<String> node: nodes) {
			node.clearDOTStyleProperties();
		}
	}

	public List<CallGraphNode<String>> asNodesList() {
		return _asNodesListLoop(getRootNode());
	}

	public List<CallGraphNode<String>> asSortedNodesList(){
		List<CallGraphNode<String>> toRet = asNodesList();
		Collections.sort(toRet);
		Collections.reverse(toRet);
		return toRet;
	}

	private List<CallGraphNode<String>> _asNodesListLoop(CallGraphNode<String> node) {
		List<CallGraphNode<String>> toRet = new ArrayList<>();

		toRet.add(node);
		for (CallGraphNode<String> child : node.getChildren()) {
			toRet.addAll(_asNodesListLoop(child));
		}
		return toRet;
	}

	public List<CallGraphNode<String>> getNormRelevantNodes(double relevancyTreshold){
		List<CallGraphNode<String>> relevantNodes = new ArrayList<>();

		for (CallGraphNode<String> node : asNodesList()) {
			if (node.getNormRelevancy() >= relevancyTreshold){
				relevantNodes.add(node);
			}
		}
		return relevantNodes;
	}


	public List<CallGraphNode<String>> getNodesWithData(String data){
		List<CallGraphNode<String>> toRet = new ArrayList<>();
		for (CallGraphNode<String> node : asNodesList()) {
			if (node.getData().equalsIgnoreCase(data))
				toRet.add(node);
		}
		return toRet;
	}

	private void __updateDepths() {
		rootNode.setDepth(0.0);
		__updateDepthsLoop(rootNode);
		double maxDepth = getMaxDepth();
		for (CallGraphNode<String> node : asNodesList()) {
			node.setDepth(node.getDepth()/maxDepth);
		}
	}

	private void __updateDepthsLoop(CallGraphNode<String> node) {
		if (node.getParent() != null)
			node.setDepth(node.getParent().getDepth() + 1);
		for (CallGraphNode<String> child : node.getChildren()) {
			__updateDepthsLoop(child);
		}
	}

	private void __updateNormRepWeight(){
		for (CallGraphNode<String> node : asNodesList()) {
			node.setNormRepWeight(node.getRepWeight()/getRootNode().getRepWeight());
		}
	}

	private void __updateMostChildrenNode(){
		setMostChildrenNode(getRootNode());

		for (CallGraphNode<String> node : asNodesList()) {
			if (node.getChildrenNumber() >= getMostChildrenNode().getChildrenNumber()){
				setMostChildrenNode(node);
			}
		}
	}

	private void __updateRelevancies() {
		double maxRelevancy = 0.0;
		List<CallGraphNode<String>> nodes = asNodesList();
		for (CallGraphNode<String> node : nodes) {
			double nodeRelevancy = node.getRelevancy();
			if (nodeRelevancy > maxRelevancy) {
				maxRelevancy = nodeRelevancy;
				setMostRelevantNode(node);
			}
		}
		for (CallGraphNode<String> node : nodes) {
			node.setNormRelevancy(node.getRelevancy());
		}

	}

	private void __updateNormChildrenNumber() {
		for (CallGraphNode<String> node : asNodesList()) {
			node.setNormChildrenNumber(((double)node.getChildrenNumber())/((double)getMostChildrenNode().getChildrenNumber()));
		}

	}

	public void updateMetrics() {
		__updateDepths();
		__updateNormRepWeight();
		__updateMostChildrenNode();
		__updateNormChildrenNumber();
		__updateRelevancies();
	}

	public void setThresholds(double repWeightThreshold, double childrenNumberThreshold, double depthThreshold){
		List<CallGraphNode<String>> nodes = asNodesList();
		for (CallGraphNode<String> callGraphNode : nodes) {
			callGraphNode.setRepWeightThreshold(repWeightThreshold);
			callGraphNode.setChildrenNumberThreshold(childrenNumberThreshold);
			callGraphNode.setDepthThreshold(depthThreshold);
		}
		updateMetrics();
		if (!dotPath.equalsIgnoreCase(""))
			toDOT(dotPath);
	}

	private boolean __snipRoot(List<String> toSnip){
		for (String string : toSnip) {
			if (getRootNode().getData().toLowerCase().contains(string.toLowerCase()) && getRootNode().getChildren().size() == 1){
				CallGraphNode<String> newRoot = getRootNode().getChildren().get(0);
				newRoot.setParent(null);
				setRootNode(newRoot); 
				return true;
			}
		}
		return false;
	}
	
	public void prune(List<String> toSnip) {
		while (__snipRoot(toSnip));
		
		for (CallGraphNode<String> callGraphNode : asNodesList()) {
			for (String string : toSnip) {
				if (callGraphNode.getData().toLowerCase().contains(string.toLowerCase())){
					callGraphNode.snipFromGraph();
					break;
				}
			}
		}
		updateMetrics();
	}

	public void setDOTStyleProperty(DOTStyleProperty dOTStyleProperty) {
		for (CallGraphNode<String> node : asNodesList()) {
			node.addDOTStyleProperty(dOTStyleProperty);
		}
	}

}