package it.unimib.disco.lta.fixLocusCandidatesIdentification.callGraphGeneration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import it.unimib.disco.lta.anomalyDetection.DiffAnalysisResult;
import it.unimib.disco.lta.anomalyDetection.SuspiciousInvocationBlock;

public class CallGraphGenerator {
	
	private static final Logger log = Logger.getLogger(CallGraphGenerator.class);


	public static final String[] NODES_TO_IGNORE = { "xposed", "beforeHookedMethod", "getStackTrace",
			"getThreadStackTrace" };

	public CallGraphGenerator() {

	}
	/*
	private boolean __checkForShortClassName(String methodString, boolean keepEmpty){
		if (methodString == null || methodString.length() == 0)
			return keepEmpty;
		int dotIndex = methodString.lastIndexOf(".");
		String classname = methodString.substring(0,dotIndex);
		classname = classname.substring(classname.lastIndexOf('.')+1);
		if (classname.length() < 4)
			return true;
		return false;
	}*/

	public CallGraph generateCallGraph(DiffAnalysisResult diffAnalysisResult, int threshold, String outputFolder, String name) {

		log.info("Generating Failure Call Tree");
		
		List<SuspiciousInvocationBlock> blocks = diffAnalysisResult.getInvocations();

		List<SuspiciousInvocationBlock> thresholdBlocks = new ArrayList<>();
		for (SuspiciousInvocationBlock suspiciousInvocationBlock : blocks) {
			if (suspiciousInvocationBlock.getElements().size() >= threshold) {
				thresholdBlocks.add(suspiciousInvocationBlock);
			}
		}
		if (thresholdBlocks.size() == 0)
			return null;
		CallGraph toRet = generateCallGraph(thresholdBlocks, name);

		if (toRet != null) {
			toRet.toDOT(outputFolder + "FULL.dot");
			List<String> toSnip = new ArrayList<>();
			toSnip.addAll(Arrays.asList(NODES_TO_IGNORE));
			log.info("pruning graph to only contain SIBs with weight > " + threshold);
			toRet.prune(toSnip);
//			toRet.setThresholds(0.22, 0.56, 0.22);
			toRet.toDOT(outputFolder + "SIB_treshold_" + threshold + ".dot");
			return toRet;
		}
		return null;
	}

	private static CallGraph generateCallGraph(List<SuspiciousInvocationBlock> blocks, String callGraphName) {
		return CallGraphFactory.getCallGraph(blocks, callGraphName);
	}

}
