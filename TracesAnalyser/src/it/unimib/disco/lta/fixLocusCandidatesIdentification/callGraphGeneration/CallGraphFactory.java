package it.unimib.disco.lta.fixLocusCandidatesIdentification.callGraphGeneration;

import java.util.List;

import it.unimib.disco.lta.anomalyDetection.SuspiciousInvocationBlock;

public class CallGraphFactory {

	public static CallGraph getCallGraph(List<SuspiciousInvocationBlock> blocks, String callGraphName){
		String rootNodeName = blocks.get(0).getRepresentative().getStackTrace();
		rootNodeName = rootNodeName.substring(rootNodeName.lastIndexOf(',')+1).trim();
		CallGraph callGraphToRet = new CallGraph(callGraphName, rootNodeName);
		CallGraphNode<String> root = callGraphToRet.getRootNode();
		callGraphToRet.setReferenceBlocks(blocks);
		for (SuspiciousInvocationBlock suspiciousInvocationBlock : blocks) {
			CallGraphNode<String> tmpNode = _nodeFactoryLoop(suspiciousInvocationBlock.getRepresentative().getStackTrace(), root);
			tmpNode.setRepWeight(suspiciousInvocationBlock.getWeight());
		}
		callGraphToRet.updateMetrics();
		return callGraphToRet;
	}

	private static CallGraphNode<String> _nodeFactoryLoop(String line, CallGraphNode<String> rootNode) {
		CallGraphNode<String> node = rootNode;
		do {
			int commaIndex = line.lastIndexOf(',');
			String newNodeData = line.substring(commaIndex + 1).trim();
			CallGraphNode<String> newNode = new CallGraphNode<String>(newNodeData);
			if (node.addChild(newNode)) {
				node = newNode;
			} else {
				node = node.getChildNodeWithData(newNodeData) == null ? node : node.getChildNodeWithData(newNodeData);
			}
			line = line.substring(0, (commaIndex == -1 ? 0 : commaIndex));

		} while (line.length() > 0);

		return node;
	}
}
