package it.unimib.disco.lta.fixLocusCandidatesIdentification.callGraphGeneration;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CallGraphNode<T> implements Comparable<CallGraphNode<T>> {
	private List<CallGraphNode<T>> children = new ArrayList<CallGraphNode<T>>();
	private CallGraphNode<T> parent = null;
	private T data = null;
	private double repWeight, normRepWeight, relevancy, depth, childrenNumber;
	private List<DOTStyleProperty> dOTStyleProperties = null;
	private double childrenNumberThreshold = 0.8, repWeightThreshold = 0.2, depthThreshold = 0.0;
	
	public double getChildrenNumberThreshold() {
		return childrenNumberThreshold;
	}
	
	public void setChildrenNumberThreshold(double childrenNumberThreshold) {
		this.childrenNumberThreshold = childrenNumberThreshold;
	}
	
	public double getRepWeightThreshold() {
		return repWeightThreshold;
	}
	
	public void setRepWeightThreshold(double repWeightThreshold) {
		this.repWeightThreshold = repWeightThreshold;
	}
	
	public double getDepthThreshold() {
		return depthThreshold;
	}
	
	public void setDepthThreshold(double depthThreshold) {
		this.depthThreshold = depthThreshold;
	}

	public enum DOTStyleProperty {
		BOLD, GREEN, ORANGE, YELLOW, RED, RELEVANCYGREYVALUE
	}

	public void setDOTStyleProperty(DOTStyleProperty dOTStyleProperty) {
		getDOTStyleProperties().clear();
		getDOTStyleProperties().add(dOTStyleProperty);
	}

	public void addDOTStyleProperty(DOTStyleProperty dOTStyleProperty) {
		if (!getDOTStyleProperties().contains(dOTStyleProperty)) {
			getDOTStyleProperties().add(dOTStyleProperty);
		}
	}

	public void clearDOTStyleProperties(){
		this.dOTStyleProperties.clear();
	}

	public List<DOTStyleProperty> getDOTStyleProperties() {
		if (this.dOTStyleProperties == null)
			this.dOTStyleProperties = new ArrayList<>();
		return this.dOTStyleProperties;
	}

	public CallGraphNode(T data) {
		this.data = data;
	}

	public CallGraphNode(T data, CallGraphNode<T> parent) {
		this(data, parent, 0.0);
	}

	public CallGraphNode(T data, CallGraphNode<T> parent, double diffWeight) {
		this.data = data;
		this.parent = parent;
		this.repWeight = diffWeight;
		depth = 0;
	}

	public double getNormChildrenNumber() {
		return childrenNumber;
	}

	public void setNormChildrenNumber(double normChildrenNumber) {
		this.childrenNumber = normChildrenNumber;
	}

	public double getNormRelevancy() {
		return relevancy;
	}

	public void setNormRelevancy(double normRelevancy) {
		this.relevancy = normRelevancy;
	}

	public double getNormRepWeight() {
		return normRepWeight;
	}

	public void setNormRepWeight(double normRepWeight) {
		this.normRepWeight = normRepWeight;
	}

	public double getDepth() {
		return depth;
	}

	public void setDepth(double depth) {
		this.depth = depth;
	}

	public double getRepWeight() {
		double repWeightsSum = repWeight;
		for (CallGraphNode<T> child : children) {
			repWeightsSum += child.getRepWeight();
		}
		return repWeightsSum;
	}

	public void setRepWeight(double repWeight) {
		this.repWeight = repWeight;
	}

	public List<CallGraphNode<T>> getChildren() {
		return children;
	}

	public int getChildrenNumber() {
		return getChildren().size();
	}
	
	public CallGraphNode<T> getParent() {
		return parent;
	}

	public void setParent(CallGraphNode<T> parent) {
		if (parent != null && !parent.contains(this))
			parent.addChild(this);
		this.parent = parent;
	}

	public CallGraphNode<T> addChild(T data) {
		CallGraphNode<T> child = new CallGraphNode<T>(data);
		if (addChild(child))
			return child;
		return null;
	}

	public boolean addChild(CallGraphNode<T> child) {
		return addChild(child, false);
	}

	public boolean addChild(CallGraphNode<T> child, boolean force) {
		if (!contains(child)) {
			__addChild(child);
			return true;
		}
		if (getData().equals(child.getData()) && force){
			for (CallGraphNode<T> childChild : child.getChildren()) {
				__addChild(childChild);
				return true;
			}
		}
		return false;
	}
	
	private void __addChild(CallGraphNode<T> child) {
		this.children.add(child);
		child.setParent(this);
		child.setDepth(this.getDepth() + 1);		
	}
	public boolean addChildren(List<CallGraphNode<T>> children){
		return addChildren(children, false);
	}
	public boolean addChildren(List<CallGraphNode<T>> children, boolean force){
		boolean toRet = true;
		for (CallGraphNode<T> callGraphNode : children) {
			toRet = toRet && addChild(callGraphNode, force);
		}
		return toRet;
	}

	public T getData() {
		return this.data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public boolean isRoot() {
		return (this.parent == null);
	}

	public boolean isLeaf() {
		if (this.children.size() == 0)
			return true;
		else
			return false;
	}

	public void removeParent() {
		this.parent.getChildren().remove(this);
		this.parent = null;
	}

	public void snipFromGraph(){
		if (this.parent != null){
			this.parent.addChildren(getChildren(), true);
			for (CallGraphNode<T> callGraphNode : children) {
				callGraphNode.setDepth(this.parent.getDepth() + 1);
			}
			this.parent.setRepWeight(getRepWeight());
			removeParent();
		} else{
			if (children.size() > 0){
			for (CallGraphNode<T> callGraphNode : children) {
				callGraphNode.setDepth(0);
				callGraphNode.setParent(null);
			}
			} else {
				
			}
		}
	}

	public boolean contains(CallGraphNode<T> toCheck) {
		return contains(toCheck, false);
	}

	public boolean contains(CallGraphNode<T> toCheck, boolean recursive) {
		if (this.equals(toCheck))
			return true;

		if (recursive) {
			for (CallGraphNode<T> callGraphNode : children) {
				if (callGraphNode.contains(toCheck, true))
					return true;
			}
		} else {
			for (CallGraphNode<T> callGraphNode : children) {
				if (callGraphNode.equals(toCheck)) {
					return true;
				}
			}
		}
		return false;
	}

	public double getRelevancy() {
		return getRelevancy(this.childrenNumberThreshold, this.repWeightThreshold, this.depthThreshold);
	}

	public double getRelevancy(double childrenNumberWeight, double diffWeightWeight, double depthWeight) {
		return getNormChildrenNumber() * childrenNumberWeight + getNormRepWeight() * diffWeightWeight
				+ getDepth() * depthWeight;
	}

	public CallGraphNode<T> getChildNodeWithData(T data) {
		for (CallGraphNode<T> callGraphNode : children) {
			if (callGraphNode.getData().equals(data))
				return callGraphNode;
		}
		return null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("unchecked")
		CallGraphNode<T> other = (CallGraphNode<T>) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CallGraphNode [children=" + children + ", parent=" + parent + ", data=" + data + ", repWeight="
				+ repWeight + ", normRepWeight=" + normRepWeight + ", relevancy=" + relevancy + ", depth=" + depth
				+ ", childrenNumber=" + childrenNumber + ", dOTStyleProperties=" + dOTStyleProperties
				+ ", childrenNumberThreshold=" + childrenNumberThreshold + ", diffWeightThreshold="
				+ repWeightThreshold + ", depthThreshold=" + depthThreshold + "]";
	}

	private String _styleStringForDOTStyleProperty() {
		List<DOTStyleProperty> localProperties = new ArrayList<>();
		localProperties.addAll(this.getDOTStyleProperties());
		if (localProperties.size() == 0) {
			return "";
		}

		String styleString = "";

		if (localProperties.contains(DOTStyleProperty.BOLD)) {
			styleString += "penwidth=10, ";
			localProperties.remove(DOTStyleProperty.BOLD);
		}
		if (localProperties.size() > 0) {
			styleString += "style=filled, ";
			switch (localProperties.get(0)) {

			case GREEN:
				styleString += "fillcolor=green";
				break;
			case ORANGE:
				styleString += "fillcolor=orange";
				break;
			case YELLOW:
				styleString += "fillcolor=yellow";
				break;
			case RED:
				styleString += "fillcolor=red";
				break;
			case RELEVANCYGREYVALUE:
				styleString += "fillcolor=" + __getHexRGBColorValue();
				break;
			default:
				break;
			}
			styleString += ", ";
		}
		return styleString;
	}

	private String __getHexRGBColorValue() {
		if (getNormRelevancy() > 0.5){
			return "\"#5E5E5E\",fontcolor=white";
		}
		if (getNormRelevancy() > 0.4){
			return "\"#919191\"";
		}
		if (getNormRelevancy() > 0.30){
			return "\"#A9A9A9\"";
		}
		if (getNormRelevancy() > 0.25){
			return "\"#C0C0C0\"";
		}
		if (getNormRelevancy() > 0.20){
			return "\"#EBEBEB\"";
		}
		return "\"#FFFFFF\"";
		
		
		
	}

	public List<String> toDOT(String iD) {
		List<String> toRet = new ArrayList<>();
		String dotProperty = _styleStringForDOTStyleProperty();
//		toRet.add("\"" + iD + "\" [" + dotProperty + 
//				"label=\"" + getData().toString() + 
//				"\\n RepWeight=" + getRepWeight() +
//				"\\n NormRepWeight=" + getNormRepWeight() +
//				"\\n ChildrenNumber=" + getNormChildrenNumber() +
//				"\\n Depth=" + getDepth() + 
//				"\\n Relevancy=" + getRelevancy() + 
//				"\\n NormRelevancy=" + getNormRelevancy() +
//				"\"];");
//		toRet.add("\"" + iD + "\" [" + dotProperty + 
//				"label=\"" + getData().toString() +  
//				"\\n NormRelevancy=" + getNormRelevancy() +
//				"\"];");
		DecimalFormat df = new DecimalFormat("#.####");
		toRet.add("\"" + iD + "\" [" + dotProperty + 
				"label=\"" + getData().toString() + 
				//"\\n ImpBlocks=" + getNormRepWeight() +
				//"\\n Children=" + getNormChildrenNumber() +
				//"\\n Depth=" + getDepth() + 
				"\\n Susp=" + df.format(getNormRelevancy()) +
				"\"];");
		for (CallGraphNode<T> child : children) {

			String childID = UUID.randomUUID().toString();

			toRet.add("\"" + iD + "\" -> \"" + childID + "\";");
			toRet.addAll(child.toDOT(childID));
		}
		return toRet;
	}

	@Override
	public int compareTo(CallGraphNode<T> o) {
		double diff = getNormRelevancy() - o.getNormRelevancy();
		if (diff < 0) {
			return -1;
		} else if (diff > 0) {
			return 1;
		} else {
			return 0;
		}
	}

}