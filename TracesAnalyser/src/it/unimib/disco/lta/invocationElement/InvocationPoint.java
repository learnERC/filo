package it.unimib.disco.lta.invocationElement;

public enum InvocationPoint{
	BEFORE,
	AFTER
}