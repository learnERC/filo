package it.unimib.disco.lta.invocationElement;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.List;
import java.util.Scanner;

import it.unimib.disco.lta.utilities.Utilities;

public class InvocationElement {
	private String methodName;
	private String invocationString;
	private String parameters;
	private String caller;
	private String stackTrace;
	private String emptyReturn;
	private String returnValue;

	private InvocationPoint invocationPoint;
	private InvocationType invocationType;
	private int line, depth_application, depth_framework;

	public int getLine() {
		return line;
	}

	public enum InvocationElementElements {
		METHODNAME, INVOCATIONSTRING, PARAMETERS, INVOCATIONPOINT, INVOCATIONTYPE, LINE, CALLER, STACKTRACE, EMPTYRETURN, AFTERSTACKTRACE, AFTERCALLER, RETURNVALUE
	}

	public static List<InvocationElement> getElements(String inputPath, int depth_application, int depth_framework) {
		ArrayList<InvocationElement> elements = new ArrayList<>();
		Scanner sc;
		try {
			sc = new Scanner(new File(inputPath));
			int line = 0;
			while (sc.hasNextLine()) {
				String next = sc.nextLine();
				if (next.indexOf("INTERNAL") != 0 && next.indexOf(' ') != -1 && (
						next.substring(0, next.indexOf(' ')).toLowerCase().contains("before") || 
						next.substring(0, next.indexOf(' ')).toLowerCase().contains("after")))
					elements.add(new InvocationElement(next, line++, depth_application, depth_framework));
			}
			elements.sort(Comparator.comparing(InvocationElement::getLine));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return elements;
	}

	public InvocationElement(String invocationString, int line, int depth_application, int depth_framework) {
		this.line = line;
		this.depth_application = depth_application;
		this.depth_framework = depth_framework;
		setInvocationString(invocationString);
	}

	private void updateElementValues() {
		_setInvocationPoint();
		if (getInvocationPoint() == InvocationPoint.BEFORE){
			_setCaller();
			_setInvocationType();
			_setParameters();
		} else{
			_setEmptyReturn();
			_setReturnValue();
		}
		_setMethodName();
		_setStackTrace();
	}

	public String getEmptyReturn() {
		if (emptyReturn == null && getInvocationPoint() == InvocationPoint.AFTER) {
			_setEmptyReturn();
		}
		return emptyReturn;
	}

	private void _setEmptyReturn() {
		try{
			int i = invocationString.indexOf("isEmpty: ");
			if (i != -1) {
				int endIndex = invocationString.indexOf(", invoked");
				if (endIndex != -1 && invocationString.indexOf(", invoked") >= i){
					emptyReturn = invocationString.substring(i, invocationString.indexOf(", invoked"));
				} else {
					emptyReturn = invocationString.substring(i);
				}
			} else {
				emptyReturn = "";
			}
		} catch (Exception e) {
			e.printStackTrace();	
		}
	}

	public String getReturnValue(){
		if (getInvocationPoint() == InvocationPoint.AFTER && returnValue == null){
			_setReturnValue();
		}
		return returnValue;
	}

	private void _setReturnValue(){
		try{
			int i = invocationString.indexOf("returnValue: ");
			if (i != -1){
				int endIndex = invocationString.indexOf(", isEmpty:");
				if (endIndex == -1){
					endIndex = invocationString.indexOf(", invoked by:");
				}
				if (endIndex != -1 && endIndex >= i){
					returnValue = invocationString.substring(i, endIndex);
				} else {
					returnValue = invocationString.substring(i);
				}
			} else {
				returnValue = "";
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	public InvocationType getInvocationType() {
		if (invocationType == null && getInvocationPoint() == InvocationPoint.BEFORE)
			_setInvocationType();
		return invocationType;
	}

	private boolean __checkForShortClassName(String methodString, boolean keepEmpty){
		if (methodString == null || methodString.length() == 0)
			return keepEmpty;
		int dotIndex = methodString.lastIndexOf(".");
		String classname = methodString.substring(0,dotIndex);
		classname = classname.substring(classname.lastIndexOf('.')+1);
		if (classname.length() <= 4)
			return true;
		return false;
	}
	
	
	// THE OUTPUT OF THE METHOD, DEPENDS ON THE DEPTH SET IN THE PARAMETERS FOR DEPTH A FRAMEWORK METHOD INVOKED BY
	// ANOTHER FRAMEWORK METHOD METHOD, COULD BECOME AN APPLICATIONTOFRAMEWORK WITH DEPTH_FRAMEWORK > 1
	private void _setInvocationType() {
		boolean methodFromFramework = false, calledByFramework = false;

		methodFromFramework = Utilities.isFrameworkMethod(getMethodName()) ||
				Utilities.isToIgnore(getMethodName());
		methodFromFramework = methodFromFramework || __checkForShortClassName(getMethodName(), true);
		
		String caller = getCaller();
		if (caller != null){
			calledByFramework = Utilities.isFrameworkMethod(caller) || Utilities.isToIgnore(caller);
		} else {
			calledByFramework = true;
		}
		calledByFramework = calledByFramework || __checkForShortClassName(caller, true);
		if (methodFromFramework && calledByFramework) {
			invocationType = InvocationType.FRAMEWORKTOFRAMEWORK;
			if (this.depth_framework > 1) {
				for (int i = 2; i <= this.depth_framework; i++) {
					String ancestor = getCaller(i);
					if (ancestor != null){
						calledByFramework = Utilities.isFrameworkMethod(ancestor) || Utilities.isToIgnore(ancestor) || __checkForShortClassName(ancestor, false);
					} else {
						calledByFramework = true;
					}
					if (calledByFramework == false) {
						invocationType = InvocationType.APPLICATIONTOFRAMEWORK;
						break;
					}
				}
			}
			
		} else if (methodFromFramework == true && calledByFramework == false) {
			invocationType = InvocationType.APPLICATIONTOFRAMEWORK;
		} else if (methodFromFramework == false && calledByFramework == true) {
			invocationType = InvocationType.FRAMEWORKTOAPPLICATION;
		} else if (methodFromFramework == false && calledByFramework == false) {
			invocationType = InvocationType.APPLICATIONTOAPPLICATION;	
			if (this.depth_application > 1) {
				for (int i = 2; i <= this.depth_application; i++) {
					String ancestor = getCaller(i);
					if (ancestor != null){
						calledByFramework = Utilities.isFrameworkMethod(ancestor) || Utilities.isToIgnore(ancestor) || __checkForShortClassName(ancestor, false);
					} else {
						calledByFramework = true;
					}
					if (calledByFramework == true) {
						invocationType = InvocationType.FRAMEWORKTOAPPLICATION;
						break;
					}
				}
			}
		}
	}
	
//	private void _setInvocationType() {
//		boolean methodFromFramework = false, calledByFramework = false;
//
//		methodFromFramework = Utilities.isFrameworkMethod(getMethodName()) ||
//				Utilities.isToIgnore(getMethodName());
//		methodFromFramework = methodFromFramework || __checkForShortClassName(getMethodName());
//		
//		String caller = getCaller();
//		if (caller != null){
//			calledByFramework = Utilities.isFrameworkMethod(getCaller()) || Utilities.isToIgnore(getCaller());
//		} else {
//			calledByFramework = true;
//		}
//		calledByFramework = calledByFramework || __checkForShortClassName(caller);
//		if (methodFromFramework && calledByFramework) {
//			invocationType = InvocationType.FRAMEWORKTOFRAMEWORK;
//		} else if (methodFromFramework == true && calledByFramework == false) {
//			invocationType = InvocationType.APPLICATIONTOFRAMEWORK;
//		} else if (methodFromFramework == false && calledByFramework == true) {
//			invocationType = InvocationType.FRAMEWORKTOAPPLICATION;
//		} else if (methodFromFramework == false && calledByFramework == false) {
//			invocationType = InvocationType.APPLICATIONTOAPPLICATION;
//		}
//	}

	public String getCaller() {
		if (caller == null)
			_setCaller();
		return caller;
	}
	
	// Returns the *level* parent from the method, so with *level == 1* it would return the parent, with *level == 2* it would return the parent's parent.
	public String getCaller(int level) {
		try{
			int beginIndex = getInvocationString().indexOf("invoked by: ");
			int endIndex = getInvocationString().indexOf(", Thread");
			if (beginIndex == -1){
				return null;
			}
			String stackTrace;
			if (endIndex != -1){
				stackTrace = getInvocationString().substring(beginIndex + 12 + level -1 , endIndex);
			} else {
				stackTrace = getInvocationString().substring(beginIndex + 12 + level -1); // 12 gets to the parent, level increase the hierarchy, and the -1 brings it back to the parent if level == 1
			}
			String[] stackTraceMethods = stackTrace.split(", ");
			String _caller = "";
			for (int i = 0; i < stackTraceMethods.length; i++){
				if (stackTraceMethods[i].contains(getMethodName()) && (i+1 < stackTraceMethods.length)){
					_caller = stackTraceMethods[i+1];
					break;
				}
			}
			return _caller;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private void _setCaller() {
		caller = getCaller(1);
	}
	
//	private void _setCaller() {
//		try{
//			int beginIndex = getInvocationString().indexOf("invoked by: ");
//			int endIndex = getInvocationString().indexOf(", Thread");
//			if (beginIndex == -1){
//				return;
//			}
//			String stackTrace;
//			if (endIndex != -1){
//				stackTrace = getInvocationString().substring(beginIndex + 12 , endIndex);
//			} else {
//				stackTrace = getInvocationString().substring(beginIndex + 12);
//			}
//			String[] stackTraceMethods = stackTrace.split(", ");
//			String _caller = "";
//			for (int i = 0; i < stackTraceMethods.length; i++){
//				if (stackTraceMethods[i].contains(getMethodName()) && (i+1 < stackTraceMethods.length)){
//					_caller = stackTraceMethods[i+1];
//					break;
//				}
//			}
//			caller = _caller;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	public String getStackTrace() {
		if (stackTrace == null)
			_setStackTrace();
		return stackTrace;
	}

	private void _setStackTrace() {
		String s = "invoked by: ";
		int endIndex = getInvocationString().indexOf(", Thread");
		if (endIndex == -1 ){
			endIndex = getInvocationString().length();
		}
		stackTrace = getInvocationString().substring(getInvocationString().indexOf(s) + s.length(), endIndex);
	}

	public InvocationPoint getInvocationPoint() {
		if (invocationPoint == null)
			_setInvocationPoint();
		return invocationPoint;
	}

	private void _setInvocationPoint() {
		// System.out.println(invocationString);
		if (getInvocationString().substring(0, getInvocationString().indexOf(" ")).equalsIgnoreCase("before")) {
			invocationPoint = InvocationPoint.BEFORE;
		} else {
			invocationPoint = InvocationPoint.AFTER;
		}
	}

	public String getMethodName() {
		if (methodName == null)
			_setMethodName();
		return methodName;
	}

	private void _setMethodName() {
		int index;
		try {
			if (getInvocationPoint() == InvocationPoint.BEFORE) {
				index = invocationString.indexOf("(");
				index = index != -1 ? index : invocationString.length();
			} else {
				int comma = invocationString.indexOf(",");

				index = comma != -1 ? comma : invocationString.length();
				int isEmptyIndex = invocationString.indexOf(", isEmpty:");
				index = (isEmptyIndex != -1) ? isEmptyIndex : index;

				int returnIndex = invocationString.indexOf(", returnValue:");
				index = (returnIndex != -1) ? returnIndex : index;
			}

			methodName = invocationString.substring(invocationString.indexOf(" ") + 1, index);
		} catch (Exception e) {
			System.err.println("Error while parsining line " + this.line);
			System.err.println(invocationString);
			e.printStackTrace();

		}

	}

	public String getInvocationString() {
		return invocationString;
	}

	public void setInvocationString(String invocationString) {
		this.invocationString = invocationString;
		//		if (invocationString.contains("isEmpty")){
		//			System.out.println("TIME TO DEBUG");
		//		}
		updateElementValues();
	}

	private void _setParameters() {
		if (invocationPoint.equals(InvocationPoint.BEFORE)) {
			int par = invocationString.indexOf("(");
			if (par == -1){
				parameters = "";
				return;
			}
			int i = invocationString.indexOf("invoked");
			int endpar;
			if (i != -1){
				endpar = invocationString.substring(0, i).indexOf(")") + 1;
			} else {
				endpar = invocationString.indexOf(")") + 1;
			}
			
			if(endpar < par){
				parameters = "";
				return;
			}
			parameters = invocationString.substring(par, endpar);
		} else {
			parameters = "";
		}
	}

	public String getParameters() {
		if (parameters == null)
			_setParameters();
		return parameters;
	}

	public boolean softEquals(InvocationElement ie2, InvocationElementElements[] iee) {
		boolean equals = true;

		for (int i = 0; i < iee.length; i++) {
			switch (iee[i]) {
			case INVOCATIONPOINT:
				equals = equals & (this.invocationPoint.equals(ie2.invocationPoint));
				break;
			case INVOCATIONSTRING:
				equals = equals & (this.invocationString.equals(ie2.invocationString));
				break;
			case INVOCATIONTYPE:
				equals = equals & (this.invocationType.equals(ie2.invocationType));
				break;
			case PARAMETERS:
				equals = equals & (this.parameters.equals(ie2.parameters));
				break;
			case METHODNAME:
				equals = equals & (this.methodName.equals(ie2.methodName));
				break;
			case LINE:
				equals = equals & (this.line == ie2.line);
				break;
			case CALLER:
				equals = equals & (this.getCaller().equals(ie2.getCaller()));
				break;
			case STACKTRACE:
				equals = equals & (this.getStackTrace().equals(ie2.getStackTrace()));
				break;
			default:
				break;
			}
			if (!equals)
				break;
		}
		return equals;
	}

	@Override
	public String toString() {
		return "[methodName= " + methodName + ", parameters= (" + parameters + "), invocationPoint= " + invocationPoint
				+ ", invocationType=" + invocationType + ", line= " + line + ", invocationString= " + invocationString
				+ "]";
	}

	public String toString(EnumSet<InvocationElementElements> elementsToConsider) {
		String ret = "";

		if (elementsToConsider.contains(InvocationElementElements.INVOCATIONPOINT)){
			ret += getInvocationPoint();
			ret += " ";
		}

		if (elementsToConsider.contains(InvocationElementElements.METHODNAME)){
			ret += getMethodName();
		}

		if (getInvocationPoint().equals(InvocationPoint.BEFORE)){
			if (elementsToConsider.contains(InvocationElementElements.PARAMETERS)){
				ret += getParameters();
			}
			String invokedBy = ", invoked by: ";
			if (elementsToConsider.contains(InvocationElementElements.CALLER)){
				ret += invokedBy + getCaller();
			} else if (elementsToConsider.contains(InvocationElementElements.STACKTRACE)){
				ret += invokedBy + getStackTrace();
			}
		} else{

			if (elementsToConsider.contains(InvocationElementElements.RETURNVALUE)){
				if (!getReturnValue().isEmpty()){
					ret += ", " + getReturnValue();
				}
			}

			if (elementsToConsider.contains(InvocationElementElements.EMPTYRETURN)){
				if (!getEmptyReturn().isEmpty()){
					ret += ", " + getEmptyReturn();
				}
			}
			String invokedBy = ", invoked by: ";
			if (elementsToConsider.contains(InvocationElementElements.AFTERCALLER)){
				ret += invokedBy + getCaller();
			} else if (elementsToConsider.contains(InvocationElementElements.AFTERSTACKTRACE)){
				ret += invokedBy + getStackTrace();
			}
		}

		return ret;
	}
}
