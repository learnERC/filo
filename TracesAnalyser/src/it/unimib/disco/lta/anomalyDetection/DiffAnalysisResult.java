package it.unimib.disco.lta.anomalyDetection;

import java.util.ArrayList;
import java.util.List;

public class DiffAnalysisResult {
	private List<SuspiciousInvocationBlock> addedInvocations, removedInvocations;

	public List<SuspiciousInvocationBlock> getAddedInvocations() {
		return addedInvocations;
	}

	public List<SuspiciousInvocationBlock> getRemovedInvocations() {
		return removedInvocations;
	}

	public DiffAnalysisResult(List<SuspiciousInvocationBlock> addedInvocations,
			List<SuspiciousInvocationBlock> removedInvocations) {
		super();
		this.addedInvocations = addedInvocations;
		this.removedInvocations = removedInvocations;
	}
	
	public List<SuspiciousInvocationBlock> getInvocations(){
		List<SuspiciousInvocationBlock> toRet = new ArrayList<>();
		toRet.addAll(getAddedInvocations());
		toRet.addAll(getRemovedInvocations());
		return toRet;
	}
}
