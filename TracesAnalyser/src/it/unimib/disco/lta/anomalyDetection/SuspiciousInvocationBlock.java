package it.unimib.disco.lta.anomalyDetection;

import java.util.List;

import it.unimib.disco.lta.invocationElement.InvocationElement;

public class SuspiciousInvocationBlock {
		
	private List<InvocationElement> elements;

	public SuspiciousInvocationBlock(List<InvocationElement> elements) {
		super();
		this.elements = elements;
	} 

	public InvocationElement getRepresentative(){
		return elements.get(0);
	}
	
	public int getWeight(){
		return elements.size();
	}
	
	public List<InvocationElement> getElements() {
		return elements;
	}
}
