package it.unimib.disco.lta.anomalyDetection;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import org.apache.log4j.Logger;

import it.unimib.disco.lta.app.FILOProperties;
import it.unimib.disco.lta.invocationElement.InvocationElement;
import it.unimib.disco.lta.invocationElement.InvocationElement.InvocationElementElements;
import it.unimib.disco.lta.traceFiltering.TraceFilter;
import it.unimib.disco.lta.utilities.Utilities;

public class AnomalyDetector {

	private static final Logger log = Logger.getLogger(AnomalyDetector.class);

	private String baselineTracePath, failureTracePath, anomalyDetectionFolder;
	private EnumSet<InvocationElementElements> elementsToConsider;
	private int depth_application, depth_framework;
	
	public static final String ANOMALY_DETECTION_TRACE_SUFFIX = "_forDIFF";
	public static final String DIFF_OUTPUT_NAME = "diff.trace";

	public AnomalyDetector(String baselineTracePath, String failureTracePath,
			FILOProperties properties, String anomalyDetectionFolder) {
		super();
		this.baselineTracePath = baselineTracePath;
		this.failureTracePath = failureTracePath;
		this.elementsToConsider = properties.elementsToConsider;
		this.anomalyDetectionFolder = anomalyDetectionFolder;
		
		this.depth_application = properties.depth_application;
		this.depth_framework = properties.depth_framework;
	}

	public DiffAnalysisResult runAnomalyDetection() {
		log.info("Filtering traces to perform DIFF tool using only selected elements (from parameters).");
		String baselineFilteredForDIFF = filterForDIFF(baselineTracePath, elementsToConsider);
		String failureFilteredForDIFF = filterForDIFF(failureTracePath, elementsToConsider);

		log.info("Running diff tools on the traces.");
		List<String> diffLines = _diff(baselineFilteredForDIFF, failureFilteredForDIFF, anomalyDetectionFolder + DIFF_OUTPUT_NAME);
		
		// Needs the original traces to re-add elements removed for the diff.
		return extractChangedElements(baselineTracePath, failureTracePath, diffLines);
				
	}
	
	private static List<String> _diff(String first, String second, String outputName) {
		try {
			String command = "diff";
			File diffFile = new File(outputName);

			Process proc = new ProcessBuilder(command, first, second)
					.redirectErrorStream(true)
					.redirectOutput(diffFile)
					.start();
			proc.getOutputStream().close();
			proc.waitFor();

			List<String> diffFileLines = Utilities.loadFile(diffFile.getAbsolutePath());
			return diffFileLines;
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}		
		return null;
	}

	private String filterForDIFF(String tracePath, EnumSet<InvocationElementElements> elementsToConsider) {
		List<InvocationElement> elements = InvocationElement.getElements(tracePath, this.depth_application, this.depth_framework);
		String outputFilePath = anomalyDetectionFolder
				+ tracePath.substring(tracePath.lastIndexOf(File.separator), tracePath.lastIndexOf('.'))
				+ ANOMALY_DETECTION_TRACE_SUFFIX + ".trace";
		log.info("WriteFile");
		Utilities.writeFile(TraceFilter.getElements(elements, elementsToConsider), outputFilePath);
		log.info(outputFilePath + " file created");
		return outputFilePath;

	}

	private DiffAnalysisResult extractChangedElements(String baselineTracePath, String failureTracePath, List<String> diffLines) {
		List<SuspiciousInvocationBlock> removedInvocations = new ArrayList<>();
		List<SuspiciousInvocationBlock> addedInvocations = new ArrayList<>();
		int numberBefore = 0, numberAfter = 0;
		int tmpIsAdded = 0;
		List<InvocationElement> tmpElementsAdded = new ArrayList<>();
		List<InvocationElement> tmpElementsRemoved = new ArrayList<>();
		
		List<InvocationElement> originalFirstFileElements = InvocationElement.getElements(baselineTracePath, this.depth_application, this.depth_framework);
		List<InvocationElement> originalSecondFileElements = InvocationElement.getElements(failureTracePath, this.depth_application, this.depth_framework);
		for (String diffLine : diffLines) {
			if (Character.isDigit(diffLine.charAt(0))){
				
				if (tmpIsAdded == 1 && tmpElementsAdded.size() > 0){
					addedInvocations.add(new SuspiciousInvocationBlock(tmpElementsAdded));
				}else if (tmpIsAdded == -1 && tmpElementsRemoved.size() > 0) {
					removedInvocations.add(new SuspiciousInvocationBlock(tmpElementsRemoved));
				}
				tmpIsAdded = 0;
				tmpElementsAdded = new ArrayList<>();
				tmpElementsRemoved = new ArrayList<>();

				String numBs = "", numAs = "";
				boolean before = true;
				for (char difflineChar : diffLine.toCharArray()) { 
					if (!Character.isAlphabetic(difflineChar)){
						if (before){
							numBs += difflineChar;
						} else{
							numAs += difflineChar;
						}
					} else {
						before = false;
					}
				}
				if (numBs.indexOf(',') != -1){
					numBs = numBs.substring(0, numBs.indexOf(','));
				}
				if (numAs.indexOf(',') != -1){
					numAs = numAs.substring(0, numAs.indexOf(','));
				}

				numberBefore = Integer.valueOf(numBs).intValue();
				
				numberAfter = Integer.valueOf(numAs).intValue();
				
				continue;
			} else{

				if (diffLine.charAt(0) == '>'){
					tmpIsAdded = 1;
					tmpElementsAdded.add(originalSecondFileElements.get(numberAfter-1));
					numberAfter++;
				} else if (diffLine.charAt(0) == '<') {
					tmpIsAdded = -1;
					tmpElementsRemoved.add(originalFirstFileElements.get(numberBefore-1));
					numberBefore++;
				} else if (diffLine.charAt(0) == '-') {
					if (tmpIsAdded == 1 && tmpElementsAdded.size() > 0){
						addedInvocations.add(new SuspiciousInvocationBlock(tmpElementsAdded));
					}else if (tmpIsAdded == -1 && tmpElementsRemoved.size() > 0) {
						removedInvocations.add(new SuspiciousInvocationBlock(tmpElementsRemoved));
					}
					tmpIsAdded = 0;
					tmpElementsAdded = new ArrayList<>();
					tmpElementsRemoved = new ArrayList<>();
				} else {
					System.err.println("ISSUE: " + diffLine);
				}
			}
		}
		
		if (tmpElementsAdded.size() > 0){
			addedInvocations.add(new SuspiciousInvocationBlock(tmpElementsAdded));
		} if (tmpElementsRemoved.size() > 0) {
			removedInvocations.add(new SuspiciousInvocationBlock(tmpElementsRemoved));
		}
		
		return new DiffAnalysisResult(addedInvocations, removedInvocations);
		
	}
}