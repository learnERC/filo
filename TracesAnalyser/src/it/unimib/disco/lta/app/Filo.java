package it.unimib.disco.lta.app;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import it.unimib.disco.lta.anomalyDetection.AnomalyDetector;
import it.unimib.disco.lta.anomalyDetection.DiffAnalysisResult;
import it.unimib.disco.lta.fixLocusCandidatesIdentification.callGraphGeneration.CallGraph;
import it.unimib.disco.lta.fixLocusCandidatesIdentification.callGraphGeneration.CallGraphGenerator;
import it.unimib.disco.lta.fixLocusCandidatesIdentification.callGraphGeneration.CallGraphNode.DOTStyleProperty;
import it.unimib.disco.lta.fixLocusCandidatesIdentification.rankingGeneration.RankingGenerator;
import it.unimib.disco.lta.invocationElement.InvocationElement.InvocationElementElements;
import it.unimib.disco.lta.invocationElement.InvocationType;
import it.unimib.disco.lta.traceFiltering.TraceFilter;
import it.unimib.disco.lta.utilities.Utilities;

public class Filo {
	/* Get the logger for the actual class name to be printed on */
	private static final Logger log = Logger.getLogger(Filo.class);
	
	/** filoTraceElements contains all the elements needed by FILO to run the analysis 
	 * and construct the failure call tree. Any change may render FILO unable to construct the tree.
	 * 
	 */		
	public static final EnumSet<InvocationElementElements> FILOTRACEELEMENTS = EnumSet.of(
			InvocationElementElements.INVOCATIONPOINT,
			InvocationElementElements.METHODNAME,
			InvocationElementElements.PARAMETERS,
			InvocationElementElements.STACKTRACE,
			InvocationElementElements.RETURNVALUE,
			InvocationElementElements.EMPTYRETURN,
			InvocationElementElements.AFTERSTACKTRACE
			);
	
	public static final EnumSet<InvocationType> INVOCATIONTYPES = EnumSet.of(
			InvocationType.APPLICATIONTOFRAMEWORK,
			InvocationType.FRAMEWORKTOAPPLICATION);
/*
	public static final double K1 = 0.25; // RepWeightThreshold
	public static final double K2 = 0.75; // DepthThreshold
	public static final double K3 = 0.0; // ChildrenNumberThreshold
*/
	public static void main(String[] args) {

		// Basic log4j configuration
		BasicConfigurator.configure();

		CLIArguments arguments = new CLIArguments();
		CmdLineParser argsParser = new CmdLineParser(arguments);
		try {
			argsParser.parseArgument(args);
			FILOProperties properties = new FILOProperties();
			if (arguments.depth_application != 1 || arguments.depth_framework != 1) {
				properties.depth_application = arguments.depth_application;
				properties.depth_framework = arguments.depth_framework;
			}
			if(arguments.methods_list != null && !arguments.methods_list.equals("")) {
				List<String> suspiciousMethods = new ArrayList<>();
				String[] suspMethods = arguments.methods_list.split(";");
				for(int i = 0; i < suspMethods.length; i++) {
					if(suspMethods[i] != null && !suspMethods[i].equals("")) {
						suspiciousMethods.add(suspMethods[i]);
					}
				}
				properties.methods_list = suspiciousMethods;
			}
			
			if(arguments.weightsFolder != null && !arguments.weightsFolder.equals("")) {
				combineRankings(arguments.weightsFolder);
				log.info("Process terminated");
			} else if((arguments.baselineTracePath != null && !arguments.baselineTracePath.equals(""))
					&& (arguments.failureTracePath != null && !arguments.failureTracePath.equals(""))
					&& (arguments.analysisFolder != null && !arguments.analysisFolder.equals(""))) {
				runAnalysis(arguments.baselineTracePath, arguments.failureTracePath, arguments.analysisFolder, properties);
				log.info("Process terminated");
			} else {
				argsParser.printUsage(System.err);
			}
		} catch (CmdLineException e) {
			argsParser.printUsage(System.err);
			log.error(e.getMessage());
		}
	}

	private static void runAnalysis(String baselineTracePath, String failureTracePath, String analysisFolder, FILOProperties properties) {
		
		log.info("Filtering traces to only contain boundary calls and elements needed by FILO");
		
		TraceFilter traceFilter = new TraceFilter(INVOCATIONTYPES, FILOTRACEELEMENTS, properties.depth_application, properties.depth_framework);
		String filteredBaseline = traceFilter.filter(baselineTracePath);
		String filteredFailure = traceFilter.filter(failureTracePath);
		
		
		/** elementsToConsider contains the elements used in the traces upon which the diff tool is run.
		 *  They can be modified at pleasure, yielding different results in the analysis.
		 */
		
		log.info("Starting anomaly detection phase");
		String anomalyDetectionFolder = analysisFolder + File.separator + properties.anomalyDetectionFolderName + File.separator;
		new File(anomalyDetectionFolder).mkdirs();
		
		AnomalyDetector anomalyDetector = new AnomalyDetector(filteredBaseline, filteredFailure, properties, anomalyDetectionFolder);
		DiffAnalysisResult diffAnalysisResult = anomalyDetector.runAnomalyDetection();

		log.info("Starting CallGraph generation phase");
		String callGraphFolder = analysisFolder + File.separator + properties.callGraphFolderName + File.separator;
		new File(callGraphFolder).mkdirs();
		CallGraphGenerator callGraphGenerator = new CallGraphGenerator();
		CallGraph failureCallTree = callGraphGenerator.generateCallGraph(diffAnalysisResult, properties.sibThreshold, callGraphFolder, "CallGraph");
		log.info("Applying parameters values for suspiciousness score");
		failureCallTree.setDOTStyleProperty(DOTStyleProperty.RELEVANCYGREYVALUE);
		
		log.info("Starting Ranking generation phase");
		String rankingFolder = analysisFolder + File.separator + properties.rankingFolderName + File.separator;
		new File(rankingFolder).mkdirs();
		
		if(properties.methods_list == null || properties.methods_list.size() == 0) {
			double K1 = 0.25; // RepWeightThreshold
			double K2 = 0.75; // DepthThreshold
			double K3 = 0.0; // ChildrenNumberThreshold
			if(properties.depth_application == 1 && properties.depth_framework == 1) {
				K1 = 0.24;
				K2 = 0.0;
				K3 = 0.76;
			} else if(properties.depth_application == 2 && properties.depth_framework == 1) {
				K1 = 0.27;
				K2 = 0.05;
				K3 = 0.68;
			} else if((properties.depth_application == 1 && properties.depth_framework == 2) || (properties.depth_application == 2 && properties.depth_framework == 2)) {
				K1 = 0.36;
				K2 = 0.12;
				K3 = 0.52;
			}
			failureCallTree.setThresholds(K1, K3, K2);
			
			RankingGenerator rankingGenerator = new RankingGenerator();
			rankingGenerator.generateRanking(failureCallTree, rankingFolder);
		} else {
			generateAllCobinations(failureCallTree, properties, rankingFolder, baselineTracePath);
		}
	}
	
	private static void generateAllCobinations(CallGraph failureCallTree, FILOProperties properties, String rankingFolder, String baselineTracePath) {
		List<String> wList = new ArrayList<>();
		wList.add("Final Score" + Utilities.csvSeparator + "Ranking Sum" + Utilities.csvSeparator + "Suspicious Methods Found" + Utilities.csvSeparator
				+ "RepWeightThreshold" + Utilities.csvSeparator + "ChildrenNumberThreshold"
				+ Utilities.csvSeparator + "DepthThreshold" + Utilities.csvSeparator + "Methods Found Info (Position-Normalized Position-Method Name-Relevancy)");
		List<String> mList = new ArrayList<>();
		mList.add("Position" + Utilities.csvSeparator + "Normalized Position" + Utilities.csvSeparator
				+ "Method Name" + Utilities.csvSeparator + "Relevancy" + Utilities.csvSeparator
				+ "RepWeightThreshold" + Utilities.csvSeparator + "ChildrenNumberThreshold"
				+ Utilities.csvSeparator + "DepthThreshold" + Utilities.csvSeparator + "TotalRankingScore");
		
		Map<Integer, ArrayList<String>> wMap = new HashMap<>();
		
		for(int i=0; i<properties.weightsValues_list.size(); i++) {
			for(int j=0; j<properties.weightsValues_list.size(); j++) {
				for(int k=0; k<properties.weightsValues_list.size(); k++) {
					double K1 = properties.weightsValues_list.get(i); // RepWeightThreshold
					double K2 = properties.weightsValues_list.get(j); // DepthThreshold
					double K3 = properties.weightsValues_list.get(k); // ChildrenNumberThreshold
					
					int kSum = (int)(K1*100) + (int)(K2*100) + (int)(K3*100);
					if(kSum == 100) {
						failureCallTree.setThresholds(K1, K3, K2);
						
						RankingGenerator rankingGenerator = new RankingGenerator();
						rankingGenerator.generateRanking(failureCallTree, properties.methods_list, wMap, mList, K1, K2, K3);
					}
				}
			}
		}
		
		for(Integer k : wMap.keySet()) {
			ArrayList<String> tmp = wMap.get(k);
			if(tmp != null) {
				Collections.sort(tmp);
				wList.addAll(tmp);
			}
		}
		String appName = baselineTracePath.substring(baselineTracePath.substring(0, baselineTracePath.lastIndexOf(".")).lastIndexOf(".")+1, baselineTracePath.lastIndexOf("_"));
		Utilities.writeFile(wList, rankingFolder + "Weights_" + appName + ".csv");
		Utilities.writeFile(mList, rankingFolder + "SuspiciousMethods.csv");
	}
	
	private static void combineRankings(String rankingsFolder) {
		Map<String, BigDecimal> finalWeights = new HashMap<>();
		File folder = new File(rankingsFolder);
		for(File fileEntry : folder.listFiles()) {
			if(fileEntry.isFile()) {
				Scanner sc;
				try {
					sc = new Scanner(fileEntry);
					if(sc.hasNextLine())
						sc.nextLine(); // read header
					while(sc.hasNextLine()) {
						String line = sc.nextLine();
						String[] values = line.split("\\|");
						BigDecimal score = BigDecimal.valueOf(Double.parseDouble(values[0]));
						String weights = values[3] + Utilities.csvSeparator + values[4] + Utilities.csvSeparator + values[5];
						if(!finalWeights.containsKey(weights)) {
							finalWeights.put(weights, score);
						} else {
							finalWeights.put(weights, finalWeights.get(weights).add(score));
						}
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		ArrayList<String> outputWeights = new ArrayList<>();
		String[] keys = finalWeights.keySet().toArray(new String[finalWeights.keySet().size()]);
		for(String k : keys) {
			double combinedScore = finalWeights.get(k).doubleValue();
			String zeros = "";
			if(combinedScore < 10)
				zeros = "000";
			else if(combinedScore < 100)
				zeros = "00";
			else if(combinedScore < 1000)
				zeros = "0";
			outputWeights.add(zeros + combinedScore + Utilities.csvSeparator + k);
		}
		Collections.sort(outputWeights);
		outputWeights.add(0, "Combined Score" + Utilities.csvSeparator + "RepWeightThreshold" + Utilities.csvSeparator
				+ "ChildrenNumberThreshold" + Utilities.csvSeparator + "DepthThreshold");
		Utilities.writeFile(outputWeights, rankingsFolder + File.separator + "CombinedScore.csv");
	}
}
