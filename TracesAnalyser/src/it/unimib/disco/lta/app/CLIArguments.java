package it.unimib.disco.lta.app;
import org.kohsuke.args4j.Option;

public class CLIArguments {
    @Option(name = "-b", aliases = { "--baseline" }, required = false,
            usage = "path of the baseline trace file.")
    public String baselineTracePath = "";
    
    @Option(name = "-f", aliases = { "--failure" }, required = false,
            usage = "path of the failure trace file.")
    public String failureTracePath = "";
   
    @Option(name = "-o", aliases = { "--output" }, required = false,
            usage = "path of the output folder for the analysis.")
    public String analysisFolder = "";
    
    @Option(name = "-da", aliases = { "--depth_application" }, required = false,
            usage = "Depth for *APPLICATION* methods in the specified trace to be considered as boundary methods, default value is 1.")
    public int depth_application = 1;
    
    @Option(name = "-df", aliases = { "--depth_framework" }, required = false,
            usage = "Depth for *FRAMEWORK* methods in the specified trace to be considered as boundary methods, default value is 1.")
    public int depth_framework = 1;

    @Option(name = "-ml", aliases = { "--methods_list" }, required = false,
            usage = "List of suspicious methods. Separates them with ';'. Example: classA.methodA;classA.methodB;classB.methodA")
    public String methods_list = "";

    @Option(name = "-w", aliases = { "--weights" }, required = false,
            usage = "path of the weights scores folder to combine")
    public String weightsFolder = "";

}
