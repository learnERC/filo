package it.unimib.disco.lta.app;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Properties;

import it.unimib.disco.lta.invocationElement.InvocationElement.InvocationElementElements;

public class FILOProperties {

	public int sibThreshold;
//	public EnumSet<InvocationType> invocationTypes;
	public EnumSet<InvocationElementElements> elementsToConsider;
	public String anomalyDetectionFolderName;
	public String callGraphFolderName = "CallGraph";
	public String rankingFolderName = "Ranking";
	public int depth_application = 1;
	public int depth_framework = 1;
	public List<String> methods_list = new ArrayList<>();
	public List<Double> weightsValues_list = new ArrayList<>();
	public String weightsFolder = "";
	
	public FILOProperties() {
		InputStream inputStream = null;
		Properties prop = new Properties();
		String propFileName = "config.properties";

		inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

		if (inputStream != null) {
			try {
				prop.load(inputStream);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		sibThreshold= Integer.valueOf(prop.getProperty("SIB_treshold", "-1")).intValue();
		
//		this.invocationTypes = parseInvocationsTypes(prop.getProperty("invocationTypes"));
		this.elementsToConsider = parseElementsToConsider(prop.getProperty("elementsToConsider"));
		this.anomalyDetectionFolderName = prop.getProperty("anomalyDetectionFolderName", "AnomalyDetection");
		this.callGraphFolderName = prop.getProperty("callGraphFolderName", "CallGraph");
		this.rankingFolderName = prop.getProperty("rankingFolderName", "Ranking");
		
		weightsValues_list.add(0.00);
		weightsValues_list.add(0.01);
		weightsValues_list.add(0.02);
		weightsValues_list.add(0.03);
		weightsValues_list.add(0.04);
		weightsValues_list.add(0.05);
		weightsValues_list.add(0.06);
		weightsValues_list.add(0.07);
		weightsValues_list.add(0.08);
		weightsValues_list.add(0.09);
		weightsValues_list.add(0.10);
		weightsValues_list.add(0.11);
		weightsValues_list.add(0.12);
		weightsValues_list.add(0.13);
		weightsValues_list.add(0.14);
		weightsValues_list.add(0.15);
		weightsValues_list.add(0.16);
		weightsValues_list.add(0.17);
		weightsValues_list.add(0.18);
		weightsValues_list.add(0.19);
		weightsValues_list.add(0.20);
		weightsValues_list.add(0.21);
		weightsValues_list.add(0.22);
		weightsValues_list.add(0.23);
		weightsValues_list.add(0.24);
		weightsValues_list.add(0.25);
		weightsValues_list.add(0.26);
		weightsValues_list.add(0.27);
		weightsValues_list.add(0.28);
		weightsValues_list.add(0.29);
		weightsValues_list.add(0.30);
		weightsValues_list.add(0.31);
		weightsValues_list.add(0.32);
		weightsValues_list.add(0.33);
		weightsValues_list.add(0.34);
		weightsValues_list.add(0.35);
		weightsValues_list.add(0.36);
		weightsValues_list.add(0.37);
		weightsValues_list.add(0.38);
		weightsValues_list.add(0.39);
		weightsValues_list.add(0.40);
		weightsValues_list.add(0.41);
		weightsValues_list.add(0.42);
		weightsValues_list.add(0.43);
		weightsValues_list.add(0.44);
		weightsValues_list.add(0.45);
		weightsValues_list.add(0.46);
		weightsValues_list.add(0.47);
		weightsValues_list.add(0.48);
		weightsValues_list.add(0.49);
		weightsValues_list.add(0.50);
		weightsValues_list.add(0.51);
		weightsValues_list.add(0.52);
		weightsValues_list.add(0.53);
		weightsValues_list.add(0.54);
		weightsValues_list.add(0.55);
		weightsValues_list.add(0.56);
		weightsValues_list.add(0.57);
		weightsValues_list.add(0.58);
		weightsValues_list.add(0.59);
		weightsValues_list.add(0.60);
		weightsValues_list.add(0.61);
		weightsValues_list.add(0.62);
		weightsValues_list.add(0.63);
		weightsValues_list.add(0.64);
		weightsValues_list.add(0.65);
		weightsValues_list.add(0.66);
		weightsValues_list.add(0.67);
		weightsValues_list.add(0.68);
		weightsValues_list.add(0.69);
		weightsValues_list.add(0.70);
		weightsValues_list.add(0.71);
		weightsValues_list.add(0.72);
		weightsValues_list.add(0.73);
		weightsValues_list.add(0.74);
		weightsValues_list.add(0.75);
		weightsValues_list.add(0.76);
		weightsValues_list.add(0.77);
		weightsValues_list.add(0.78);
		weightsValues_list.add(0.79);
		weightsValues_list.add(0.80);
		weightsValues_list.add(0.81);
		weightsValues_list.add(0.82);
		weightsValues_list.add(0.83);
		weightsValues_list.add(0.84);
		weightsValues_list.add(0.85);
		weightsValues_list.add(0.86);
		weightsValues_list.add(0.87);
		weightsValues_list.add(0.88);
		weightsValues_list.add(0.89);
		weightsValues_list.add(0.90);
		weightsValues_list.add(0.91);
		weightsValues_list.add(0.92);
		weightsValues_list.add(0.93);
		weightsValues_list.add(0.94);
		weightsValues_list.add(0.95);
		weightsValues_list.add(0.96);
		weightsValues_list.add(0.97);
		weightsValues_list.add(0.98);
		weightsValues_list.add(0.99);
		weightsValues_list.add(1.00);
	}
	
//	private EnumSet<InvocationType> parseInvocationsTypes(String invocationTypesString) {
//		List<InvocationType> invocationTypesList = new ArrayList<>();
//		for (String invocationTypeString : invocationTypesString.replaceAll("\\s+","").split(",")) {
//			//System.out.println(invocationTypeString);
//			invocationTypesList.add(InvocationType.valueOf(invocationTypeString));
//		}	
//		EnumSet<InvocationType> invocationTypesSet = EnumSet.copyOf(invocationTypesList);
//		return invocationTypesSet;
//	}
	
	private EnumSet<InvocationElementElements> parseElementsToConsider(String elementsToConsiderString){
		List<InvocationElementElements> elementsToConsiderList = new ArrayList<>();
		for (String elementToConsider : elementsToConsiderString.replaceAll("\\s+","").split(",")) {
			//System.out.println(invocationTypeString);
			elementsToConsiderList.add(InvocationElementElements.valueOf(elementToConsider));
		}	
		EnumSet<InvocationElementElements> elementsToConsiderSet = EnumSet.copyOf(elementsToConsiderList);
		return elementsToConsiderSet;
		
	}
}
