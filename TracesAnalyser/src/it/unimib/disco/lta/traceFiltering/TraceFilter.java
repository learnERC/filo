package it.unimib.disco.lta.traceFiltering;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Stack;

import org.apache.log4j.Logger;

import it.unimib.disco.lta.invocationElement.InvocationElement;
import it.unimib.disco.lta.invocationElement.InvocationElement.InvocationElementElements;
import it.unimib.disco.lta.invocationElement.InvocationPoint;
import it.unimib.disco.lta.invocationElement.InvocationType;
import it.unimib.disco.lta.utilities.Utilities;

public class TraceFilter {
	private static final Logger log = Logger.getLogger(TraceFilter.class);

	private EnumSet<InvocationType> invocationTypesToConsider;
	private EnumSet<InvocationElementElements> elementsToConsider;
	private int depth_application, depth_framework;
	private static final String filteredAppend = "FILTERED";


	public TraceFilter(EnumSet<InvocationType> invocationTypesToConsider,
			EnumSet<InvocationElementElements> elementsToConsider, int depth_application, int depth_framework) {
		this.invocationTypesToConsider = invocationTypesToConsider;
		this.elementsToConsider = elementsToConsider;
		this.depth_application = depth_application;
		this.depth_framework = depth_framework;
	}
	
	public String filter(String tracePath) {
		String outputPath = tracePath.substring(0, tracePath.lastIndexOf(".")) + filteredAppend
				+ tracePath.substring(tracePath.lastIndexOf("."));
		return filter(tracePath, outputPath);
	}
	
	public String filter(String tracePath, String outputPath) {
		log.info("Filtering " + tracePath);
		Stack<InvocationElement> toKeep = new Stack<>();
		File outputTraceFile = new File(outputPath);
		FileOutputStream outputStream;
		List<InvocationElement> elements = InvocationElement.getElements(tracePath, this.depth_application, this.depth_framework);
		try {
			outputStream = new FileOutputStream(outputTraceFile);

			BufferedWriter outputTraceWriter = new BufferedWriter(new OutputStreamWriter(outputStream));

			int i = 0;
			while (i < elements.size()) {
				InvocationElement element = elements.get(i);
				removeFilesInformationFromStackTrace(element);

				if (element.getInvocationPoint() == InvocationPoint.BEFORE) {
					if (invocationTypesToConsider.contains(element.getInvocationType())) {
						toKeep.push(element);
						outputTraceWriter.write(element.toString(elementsToConsider));
						outputTraceWriter.newLine();
						outputTraceWriter.flush();
					}
				} else {
					if (!toKeep.isEmpty() && toKeep.peek().getMethodName().equals(element.getMethodName())) {
						outputTraceWriter.write(element.toString(elementsToConsider));
						outputTraceWriter.newLine();
						outputTraceWriter.flush();
						toKeep.pop();
					}

				}
				i++;
			}
			outputTraceWriter.close();
			log.info(outputPath.substring(outputPath.lastIndexOf(File.separator) + 1) + " File created.");
			return outputTraceFile.getAbsolutePath();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private void removeFilesInformationFromStackTrace(InvocationElement element) {
		String invocationString = element.getInvocationString();
		int stacktraceIndex = invocationString.indexOf("invoked by");
		if (stacktraceIndex == -1)
			return;
			
		String tmpInvocationString = invocationString.substring(stacktraceIndex);
		
		long open = invocationString.chars().filter(ch -> ch == '(').count();
		long close = invocationString.chars().filter(ch -> ch == ')').count();
		if(open == close) {
			int parIndex = tmpInvocationString.indexOf('(');
			while (parIndex != -1) {
				int endParIndex = tmpInvocationString.indexOf(')');
				if(endParIndex >= parIndex) {
					String tmpParCheck = tmpInvocationString.substring(parIndex + 1, endParIndex);
					if (tmpParCheck.contains("(")) {
						parIndex = tmpInvocationString.indexOf('(', parIndex + 1);
					}
					tmpInvocationString = tmpInvocationString.substring(0, parIndex)
							+ tmpInvocationString.substring(endParIndex + 1);
					parIndex = tmpInvocationString.indexOf('(');
				} else {
					tmpInvocationString = "";
					parIndex = -1;
				}
			}
		} else {
			tmpInvocationString = "";
		}
		String newInvocationString = invocationString.substring(0, stacktraceIndex) + tmpInvocationString;
		element.setInvocationString(newInvocationString);
	}

	public static void clearPreviousFilteredTraces(String filePath) {
		File file = new File(filePath);
		if (file.isDirectory()) {
			for (String child : file.list()) {
				clearPreviousFilteredTraces(file.getAbsolutePath() + File.separator + child);
			}
		} else if (file.getName().contains(filteredAppend)) {
			Utilities.runtimeExec("rm " + file.getAbsolutePath());
		}
	}
	
	public static List<String> getElements(List<InvocationElement> elements, EnumSet<InvocationElementElements> elementsToConsider){
		List<String> returnElements = new ArrayList<>();
		for (InvocationElement invocationElement : elements) {
			returnElements.add(invocationElement.toString(elementsToConsider));
		}
		return returnElements;
	}

}
