package it.unimib.disco.lta.tracerenabler;

/**
 * Created by Marco Mobilio on 25/01/2018.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import dalvik.system.PathClassLoader;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

import android.app.Activity;
import android.app.AndroidAppHelper;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;


public class TracerEnabler implements IXposedHookLoadPackage{
    //public static final String filesDir = Environment.getDataDirectory() + "/data/it.unimib.disco.lta.tracerenabler/files/";

    @Override
    public void handleLoadPackage(final XC_LoadPackage.LoadPackageParam lpparam) throws Throwable {
        StrictMode.ThreadPolicy old = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(old)
                .permitDiskWrites()
                .permitDiskReads()
                .build());

        boolean toLog = false;
        String appToLog = "org.asdtm.goodweatherbug";
        //XposedBridge.log(lpparam.packageName + " loaded.");

        if (lpparam.packageName.equalsIgnoreCase(appToLog)){
            toLog = true;

        }

        if (toLog) {
            XposedBridge.log(lpparam.packageName + " is going to be traced.");
            final int[] recording = {0};
            final Timer[] t = new Timer[1];
            final boolean[] tracing = {false};
            XposedHelpers.findAndHookMethod(Activity.class, "onCreate",Bundle.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    XposedBridge.log("BEFORE ONCREATE ACTIVITY");
                    XposedBridge.log("first onCreate hook");
                    if (recording[0] == 0 && t[0] != null){
                        XposedBridge.log("Seems like there's a stop watchdog going on, shutting it down...");
                        t[0].cancel();
                        t[0].purge();
                        XposedBridge.log("WatchDog disabled");

                    }
                    recording[0]++;
                    XposedBridge.log("There are currently " + recording[0] + " activities running");

                    super.beforeHookedMethod(param);
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                }
            });
            XposedHelpers.findAndHookMethod(Activity.class, "onResume", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    XposedBridge.log("ONRESUME");
                    if (recording[0] == 0 && t[0] != null){
                        XposedBridge.log("Seems like there's a stop watchdog going on, shutting it down...");
                        t[0].cancel();
                        t[0].purge();
                        XposedBridge.log("WatchDog disabled");

                    }
                    recording[0] = recording[0] == 0 ? 1 : recording[0];
                    XposedBridge.log("There are currently " + recording[0] + " activities running");
                    super.beforeHookedMethod(param);
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                }
            });

            XposedHelpers.findAndHookMethod(Activity.class, "onDestroy", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    XposedBridge.log("BEFORE ONDESTROY ACTIVITY");
                    super.beforeHookedMethod(param);
                }
            });
            XposedHelpers.findAndHookMethod(Activity.class, "onPause", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    super.beforeHookedMethod(param);
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                    XposedBridge.log("AFTER ONPAUSE ACTIVITY");
                    recording[0]--;
                    XposedBridge.log("There are currently " + recording[0] + " activities running");
                    if (recording[0] == 0){
                        XposedBridge.log("Starting stop watchdog");
                        TimerTask ttask = new TimerTask() {
                            @Override
                            public void run() {
                                Debug.stopMethodTracing();
                                XposedBridge.log("TRACER STOPPED");
                                tracing[0] = false;
                            }
                        };
                        t[0] = new Timer();
                        t[0].schedule(ttask, 1000);
                    }
                }
            });
            XposedHelpers.findAndHookMethod(Application.class, "onCreate", new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    StrictMode.ThreadPolicy old = StrictMode.getThreadPolicy();
                    StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(old)
                            .permitDiskWrites()
                            .permitDiskReads()
                            .build());
                    XposedBridge.log("BEFORE ONCREATE APPLICATION");
                    if (recording[0] == 0 && tracing[0] == false){
                        tracing[0] = true;
                        //String tracePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + lpparam.packageName + "_" + Build.VERSION.SDK_INT + "_" + Calendar.getInstance().getTimeInMillis() +  ".trace";
                        //File trace = new File(tracePath);
                        //trace.createNewFile();
                        //String tracePath = lpparam.appInfo.dataDir + File.separator + lpparam.packageName + "_" + Build.VERSION.SDK_INT + "_" + Calendar.getInstance().getTimeInMillis() +  ".trace";
                        String tracePath =  ((Context)AndroidAppHelper.currentApplication()).getFilesDir().getAbsolutePath() + "/" + lpparam.packageName + "_" + Build.VERSION.SDK_INT + "_" + Calendar.getInstance().getTimeInMillis() + ".trace";
                        XposedBridge.log("Trace to be created: " + tracePath);
                        Debug.startMethodTracing(tracePath, 1024000000);

                        XposedBridge.log("TRACER STARTED");
                    } else{
                        XposedBridge.log("Tracer is already running");
                    }
//                    recording[0]++;
                    super.beforeHookedMethod(param);
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    super.afterHookedMethod(param);
                }
            });
            Class<?> exceptionHandler = Thread.getDefaultUncaughtExceptionHandler().getClass();
            XposedHelpers.findAndHookMethod(exceptionHandler, "uncaughtException", Thread.class, Throwable.class, new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                    XposedBridge.log("UNCAUGHT EXCEPTION RAISED, STOPPING THE TRACER");
                    super.beforeHookedMethod(param);
                    Debug.stopMethodTracing();
                    XposedBridge.log("TRACER STOPPED");
                }
            });
        }
    }
}
