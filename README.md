# FILO

FILO is a tool that can recommend the method that must be changed to implement the fix from the analysis of a single failing execution. FILO can also select key symptomatic anomalous events that can help the developer understanding the reason of the failure and facilitate the implementation of the fix.
Our evaluation with multiple known compatibility problems introduced by Android upgrades shows that FILO can effectively and efficiently identify the faulty methods in the apps.

## Getting Started

In order to use FILO you will need to have an Appium test case that highlights the bug you wish to analyze. The test should run succesfully with the older environment and fail when run in the updated environment.

## Prerequisites

In order to run FILO you need

- Java (to run the analysis of the traces)
- A proper test case (that highlights the bug in the new environment)
- Appium (to run the test case)
- The list of methods you whish to monitor

Furthermore, if you want to reproduce UniMiB's experiments you will need:

- Genymotion (to run the emulators)
- A Genymotion VM with the older version of the Android API and the Xposed Framework installed and running
- A Genymotion VM with the newer version of the Android API and the Xposed Framework installed and running
- IntellyJ Idea (to run the provided test cases)

## Tool Execution

### Methods to monitor
Since instrumenting every method the app and the Android Framework would be prohibitive and would introduce a lot of overhead, FILO selectively instruments only the methods that may occur as boundary calls of the analyzed test case.

If you already have the list of methods you wish to instrument and monitor, skip to the next section.

To obtain this list of methods we run the test case with the lightweight Android tracer, which inexpensively produces the list of all the executed methods. We developed a custom Xposed module that is able to selectively activate and deactivate the native Android Tracer for specific apps.

Therefore, the first step, consists in executing the test case on both environments, with the **AndroidTracerEnabler** Xposed module active on the devices.

Once the test finished, the traces produced by the Android Tracer may be pulled from the device using **ADB**. The path of the trace is shown on the device console.

The JSON file describing the boundary methods can be created by exploiting the features of the **FILO_Extra** executable file, the syntax is as follows:

```
FILOMachine:~ root$ java -jar FILO_Extras.jar
 -d (--dmtracedump) VAL : path of the dmtracedump command
 -j (--JSON)            : To create JSONs of methods to instrument
 -t (--tracePath) VAL   : path of the trace used as input. In case a folder is
                          specified, all ".trace" files will be analysed.
```
the `dmtracedump` tool is usually installed with the Android SDK, its path can be identified by running `which dmtracedump`.
An example invocation is:
```
FILOMachine:~ root$ java -jar FILO_Extras.jar -j -t org.asdtm.goodweatherbug_22_1575967939609.trace -d /Users/username/Library/Android/sdk/platform-tools/dmtracedump
```
The JSON output will be placed in the same folder as the input trace.

### Running the Logger

The Logger is a second custom Xposed module that exploits the JSON output of the previous step, in order to instrument only the boundary methods and produce more detailed traces about those methods.

Once the test ended, the traces can be pulled the same way as the previous traces.

### Running the Trace Analyser

The ***TraceAnalyser*** is the main executable of FILO. It exploits the two traces obtained by the logger, its syntax is as follows:
```
FILOMachine:~ root$ java -jar TraceAnalyser.jar 
 -b (--baseline) VAL         : path of the baseline trace file.
 -f (--failure) VAL          : path of the failure trace file.
 -o (--output) VAL           : path of the output folder for the analysis.
 -da (--depth_application) N : depth for *APPLICATION* methods in the specified trace
                               to be considered as boundary methods,
                               default value is 1.
 -df (--depth_framework) N   : depth for *FRAMEWORK* methods in the specified trace
                               to be considered as boundary methods,
                               default value is 1.
```
an example invocation is as follows:
```
FILOMachine:~ root$ java -jar TraceAnalyser.jar -da 2 -b baseline.trace -f failure.trace -o outputFolder
```
If a Java heap memory error happen, it can be solved adding `-Xmx8192m` as argument.

The final Ranking, the supporting evidence, and the Failure Call Tree will be saved in the `outputFolder` location.

A video, showcasing an example of how to run FILO is available at: <https://youtu.be/WDvkKj-wnlQ>

Computing the best set of weights for an app can be done adding the following input argument:
```
-ml (--methods_list) VAL : List of suspicious methods. Separates them with ';'.
```
a usage example is as follows:
```
FILOMachine:~ root$ java -jar TraceAnalyser.jar -b baseline.trace -f failure.trace -o outputFolder -ml "classA.methodA;classA.methodB;classB.methodA"
```
The score for each set will be saved in the `outputFolder` location.

The score of the sets of weights obtained for each app can be combined with the following syntax:
```
FILOMachine:~ root$ java -jar TraceAnalyser.jar
 -w (--weights) VAL : path of the weights scores folder to combine
```
an example is as follows:
```
FILOMachine:~ root$ java -jar TraceAnalyser.jar -w weightsFolder
```
The combined scores will be placed in the same folder as the input one.

## UniMiB Experiments

The applications needed to reproduce the experiments performed in 

_Mobilio, Marco, et al. "FILO: FIx-LOcus Recommendation for Problems Caused by Android Framework Upgrade."_

are available in the following repo:

- <https://gitlab.com/learnERC/upgraderepository>

Moreover, the actual resulting traces used in the paper and all the results of the analysis are available at:

- <https://gitlab.com/learnERC/filo_experiments>

