package it.unimib.disco.lta.unimiblogger;

import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import dalvik.system.PathClassLoader;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.SELinuxHelper;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;
import de.robv.android.xposed.services.BaseService;

import static de.robv.android.xposed.XposedHelpers.findClass;

/**
 * Created by Marco Mobilio on 27/07/2018.
 */

public class UniMiBLogger implements IXposedHookLoadPackage {
    public static final String filesDir = Environment.getDataDirectory() + "/data/it.unimib.disco.lta.unimiblogger/files";
    //String filesDir =Environment.getExternalStorageDirectory().getAbsolutePath();

    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam lpparam) throws Throwable {
//        File conf = null;
//        BufferedReader br = null;
//        try {
//            conf = new File(filesDir + "packages.txt");
//            br = new BufferedReader(new FileReader(conf));
//        }catch (FileNotFoundException fnfe){
//            conf = new File(filesDir + "packages.txt");
//            conf.createNewFile();
//            br = new BufferedReader(new FileReader(conf));
//            XposedBridge.log("Unable to locate packages.txt, an empty file has been created.");
//        }
//        String line = br.readLine();

        boolean toLog = false;
        if (lpparam.packageName.equalsIgnoreCase("org.asdtm.goodweatherbug"))
            toLog = true;
//        while (!toLog && line != null) {
//            if (line.equalsIgnoreCase(lpparam.packageName)){
//                toLog = true;
//            }
//            line = br.readLine();
//        }
        if (!toLog){
            return;
        }

        XposedBridge.log("Process " + lpparam.packageName + " is going to be logged.");
        XposedBridge.log("API Level: " + Build.VERSION.SDK_INT);
        Map<String, List> methods;
        String jsonFilePath = "";
        try {
            jsonFilePath = filesDir + "/JSONS/" + Build.VERSION.SDK_INT + "/" + lpparam.packageName +".json";
            File jsonFile = new File(jsonFilePath);
            methods = loadMethodsList(new FileInputStream(jsonFile));
        }catch (Exception e){
            e.printStackTrace();
            XposedBridge.log("Unable to load methods list (" + jsonFilePath + ") for the process, exiting.");
            return;
        }

        List<String> classes = new ArrayList<String>(methods.keySet());
        String folderName = lpparam.appInfo.dataDir + File.separator + "files";

        final String fileName = folderName + File.separator + lpparam.packageName + "_" + Build.VERSION.SDK_INT + ".trace";
        new File(folderName).mkdirs();
        XposedBridge.log(fileName);
        FileWriter fw = new FileWriter(fileName, true);
        fw.flush();
        fw.close();
        XposedBridge.log("Beginning Instrumentation, " +  classes.size() + " classes will be instrumented.");
        for (String classString : classes) {
//            try {
//                final Class<?> cResolver = findClass(classString, lpparam.classLoader);
//                List<String> methodsList = (ArrayList<String>) methods.get(classString);
//                XposedBridge.log(methodsList.size() + " methods will be instrumented in class " + classString + ".");
//                for (final String method : methodsList) {
//                    XposedBridge.hookAllMethods(cResolver, method, new MyMethodHook(cResolver,method,fileName));
//                }
//            } catch (Exception e) {
//                System.err.println("unable to load class: " + classString + ", skipping.");
//            }
            Class<?> tmpClass = null;
            try {
                tmpClass = findClass(classString, lpparam.classLoader);
                //final Class<?> cResolver = findClass(classString, lpparam.classLoader);

            } catch (XposedHelpers.ClassNotFoundError e) {
                XposedBridge.log("unable to load class: " + classString + ", skipping.");
            }
            if (tmpClass != null) {
                final Class<?> cResolver = tmpClass;
                List<String> methodsList = (ArrayList<String>) methods.get(classString);
                XposedBridge.log(methodsList.size() + " methods will be instrumented in class " + classString + ".");
                for (final String method : methodsList) {
                    try {
                        XposedBridge.hookAllMethods(cResolver, method, new MyMethodHook(cResolver, method, fileName));
                    } catch (java.lang.IllegalArgumentException e){
                        XposedBridge.log("Unable to instrument method " + method +", skipping.");
                    }
                }
            } else {
                XposedBridge.log("unable to load class: " + classString + ", skipping.");
            }

        }
        XposedBridge.log("Instrumentation finished, happy tracing.");

    }

    private Map<String, List> loadMethodsList(InputStream is) {
        Map<String, List> methods = new HashMap<>();

        JSONParser parser = new JSONParser();
        JSONObject methodsList;
        try {
            methodsList = (JSONObject) parser.parse(new InputStreamReader(is));
            JSONArray classes = (JSONArray) methodsList.get("classes");

            Iterator i = classes.iterator();
            while (i.hasNext()) {
                JSONObject jsonClass = (JSONObject) i.next();

                String className = (String) jsonClass.get("className");
                methods.put(className, new ArrayList());

                JSONArray jsonMethods = (JSONArray) jsonClass.get("methods");
                Iterator j = jsonMethods.iterator();
                while (j.hasNext()) {
                    JSONObject jsonMethod = (JSONObject) j.next();
                    methods.get(className).add(jsonMethod.get("methodName"));
                }
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return methods;
    }

}
