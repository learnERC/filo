package it.unimib.disco.lta.unimiblogger;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;

/**
 * Created by Marco Mobilio on 31/08/2017.
 */

public class MyMethodHook extends XC_MethodHook {

//    public static int depth = 0;
    private Class<?> cResolver;
    private String method, fileName;
    private StackTraceElement[] stackTrace;

    public MyMethodHook(Class<?> cResolver, String method, String fileName) {
        this.cResolver = cResolver;
        this.method = method;
        this.fileName = fileName;
    }

    private String getCallers() {
        return getCallers(-1);
    }

    private String getCallers(int depth) {
        String ret = "";

        StackTraceElement[] elements = this.stackTrace;
        depth = (depth == -1) ? elements.length : depth;
        for (int i = 0; i < depth; i++) {
            ret += elements[i].toString() + ", ";
        }

        return ret.substring(0, ret.length() - 2);
    }


    private boolean isInternalCall(){
        StackTraceElement[] elements = this.stackTrace;
        int countBefore = 0;
        for (int i = 0; i < elements.length; i++) {
            if (elements[i].toString().contains("beforeHookedMethod") || elements[i].toString().contains("afterHookedMethod")) {
                countBefore++;
                if (countBefore > 1) {
                    return true;
                }
            }
        }
        return false;

    }
//    private boolean isInternalCall(){
//        StackTraceElement[] elements = this.stackTrace;
//        ArrayList<StackTraceElement> toConsider = new ArrayList<>();
//        for (int i = 5; i < elements.length-1; i++){
//            toConsider.add(elements[i]);
//        }
//
//        for (StackTraceElement el: toConsider) {
//            if (el.toString().contains("it.unimib.disco.lta.logger.MyMethodHook.beforeHookedMethod") ||
//                    el.toString().contains("it.unimib.disco.lta.logger.MyMethodHook.afterHookedMethod")) {
//                return true;
//            }
//        }
//        if (method.equals("toString")) {
//            XposedBridge.log("EXTERNAL\n StackTrace: " + Arrays.toString(elements)+"\n");
//            XposedBridge.log("Considered: " + toConsider.toString());
//
//        }



    private boolean isEmptyImplemented(Class c) {
        for (Method method : c.getMethods()) {
            if (method.getName().equals("isEmpty")) {
                return true;
            }
        }
        return false;
    }

    //private String fileName = dirPath + "/" + lpparam.packageName + ".trace";

    @Override
    protected void beforeHookedMethod(MethodHookParam param) {
        try {
            super.beforeHookedMethod(param);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
//        if (method.equals("onCreate")){
//            XposedBridge.log("ONCREATE FOUND AND LOGGED");
//        }
        stackTrace = Thread.currentThread().getStackTrace();
//        XposedBridge.log("ENTER " + method + " Depth = " + depth);
//        if (++depth > 1)
//            return;
        if (isInternalCall())
            return;
        FileWriter fw = null;
        try {
            fw = new FileWriter(fileName, true);
        if (isInternalCall())
            return;
            //fw.write("INTERNAL ");
        fw.write("before " + cResolver.getName() + "." + method + "(");
        if (param.args != null) {
            String paramsString = "";
            for (int i = 0; i < param.args.length; i++) {

                if (param.args[i] != null) {
                    Class paramClass = param.args[i].getClass();

                    if (paramClass == Float.class || paramClass == String.class || paramClass == Integer.class
                            || paramClass == Double.class || paramClass == Boolean.class) {
                        paramsString += param.args[i].toString() + ",";
                    } else {
                        paramsString += paramClass.getName() + ",";
                    }
                }
            }
            if (paramsString.length() > 0)
                fw.write(paramsString.substring(0, paramsString.length() - 1));
        }
        fw.write(")");
        fw.write(", invoked by: " + getCallers());
        fw.write(", Thread: " + getThread());
        fw.write("\n");
        fw.flush();
        fw.close();
        } catch (IOException e) {
            XposedBridge.log("METHOD: " + method);
            e.printStackTrace();
        }
    }

    private String getThread() {
        return Thread.currentThread().getName();
    }

    @Override
    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
        super.afterHookedMethod(param);

        if (isInternalCall())
            return;

        FileWriter fw = new FileWriter(fileName, true);
        if (isInternalCall())
            return;;
            //fw.write("INTERNAL ");
        fw.write("after " + cResolver.getName() + "." + method);
        if (param.getResult() != null) {
            String returnValue = "";

            if (param.getResult().toString().contains("@")) {
                returnValue = param.getResult().getClass().getName();
            } else {
                returnValue = param.getResult().toString().replaceAll("[\\t\\n\\r]+"," ");

                //returnValue = param.getResult().toString();
            }

            fw.write(", returnValue: " + returnValue);
            if (isEmptyImplemented(param.getResult().getClass())) {
                fw.write(", isEmpty: " + param.getResult().getClass().getMethod("isEmpty").invoke(param.getResult()));
            }
        }

        //****
        fw.write(", invoked by: " + getCallers());
        fw.write(", Thread: " + getThread());
        //****
        fw.write("\n");
        fw.flush();
        fw.close();
    }

}
