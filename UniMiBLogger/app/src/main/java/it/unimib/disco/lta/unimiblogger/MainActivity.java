package it.unimib.disco.lta.unimiblogger;

import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainActivity extends ListActivity implements View.OnClickListener{
    private TextView text;
    private List<String> packagesList;
    private ArrayAdapter<String> myAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (TextView) findViewById(R.id.mainText);
        SharedPreferences spref = getSharedPreferences("PackagesPref", 0);



//        SharedPreferences spref = PreferenceManager.getDefaultSharedPreferences(this);
        Set<String> packages = spref.getStringSet("packages", new HashSet<String>());

        packagesList = new ArrayList<>();

        packagesList.addAll(packages);

        // initiate the listadapter
        myAdapter = new ArrayAdapter<String>(this,
                R.layout.row_layout, R.id.listText, packagesList);
        // assign the list adapter
        setListAdapter(myAdapter);

        FloatingActionButton myFab = (FloatingActionButton) this.findViewById(R.id.floatingActionButton);
        myFab.setOnClickListener(this);
    }

    // when an item of the list is clicked
    @Override
    protected void onListItemClick(ListView list, View view, int position, long id) {
        super.onListItemClick(list, view, position, id);

        String selectedItem = (String) getListView().getItemAtPosition(position);
    }

    @Override
    public void onClick(View v) {
        final EditText txtUrl = new EditText(v.getContext());

        // Set the default text to a link of the Queen
        txtUrl.setHint("com.example");

        new AlertDialog.Builder(v.getContext())
                .setTitle("Insert Package Name")
                .setView(txtUrl)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String newPackage = txtUrl.getText().toString();
                        SharedPreferences spref = getApplicationContext().getSharedPreferences("PackagesPref", 0);
                        SharedPreferences.Editor editor = spref.edit();
                        Set<String> packages = spref.getStringSet("packages", new HashSet<String>());

                        packages.add(newPackage);
                        editor.remove("packages");
                        editor.putStringSet("packages", packages);
                        editor.commit();

                        packagesList.add(newPackage);
                        myAdapter.notifyDataSetChanged();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                })
                .show();
    }
}
