package it.unimib.disco.lta.app;

import java.io.File;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import it.unimib.disco.lta.androidTracesAnalysis.AndroidTraceAnalyser;

public class App {
	/* Get the logger for the actual class name to be printed on */
	private static final Logger log = Logger.getLogger(App.class);

	public static void main(String[] args) {

		// Basic log4j configuration
		BasicConfigurator.configure();

		CLIArguments arguments = new CLIArguments();
		CmdLineParser argsParser = new CmdLineParser(arguments);
		
		try {
			argsParser.parseArgument(args);
			
			switch (arguments.functionality) {
			case JSON:
				log.info("JSON Creation mode.");
				createJSONFile(arguments);
				break;
			default:
				break;
			}
			
		} catch (CmdLineException e) {
			argsParser.printUsage(System.err);
			log.error(e.getMessage());
		}
	}
	
	public static void createJSONFile(CLIArguments arguments) {
		File tracePathFile = new File(arguments.tracePath);
		AndroidTraceAnalyser analyser = new AndroidTraceAnalyser(arguments.dmtracedump, arguments.depth_application, arguments.depth_framework);
		if (tracePathFile.isDirectory()) {
			analyser.parseDirectory(tracePathFile);
		} else if (arguments.tracePath.lastIndexOf('.') != -1 && arguments.tracePath.substring(arguments.tracePath.lastIndexOf('.')).equalsIgnoreCase(".trace")){
			analyser.parseFile(tracePathFile);
		}
	}
}
