package it.unimib.disco.lta.app;
import org.kohsuke.args4j.Option;

public class CLIArguments {
	
	public enum Functionality{JSON, NOTHING};
	public Functionality functionality = Functionality.NOTHING;
	
    @Option(name = "-j", aliases = { "--JSON" }, required = true,
            usage = "To create JSONs of methods to instrument")
    public void json(boolean json) {
    	if (json == true) {
    		this.functionality = Functionality.JSON;
    	}
	}
	
    @Option(name = "-d", aliases = { "--dmtracedump" }, required = true,
            usage = "path of the dmtracedump command")
    public String dmtracedump = "~/Library/Android/sdk/platform-tools/dmtracedump";
    
    @Option(name = "-t", aliases = { "--tracePath" }, required = true,
            usage = "path of the trace used as input. In case a folder is specified, all \".trace\" files will be analysed.")
    public String tracePath = "";
    
    @Option(name = "-da", aliases = { "--depth_application" }, required = false,
            usage = "Depth for *APPLICATION* methods in the specified trace to be considered as boundary methods, default value is 1.")
    public int depth_application = 1;
    
    @Option(name = "-df", aliases = { "--depth_framework" }, required = false,
            usage = "Depth for *FRAMEWORK* methods in the specified trace to be considered as boundary methods, default value is 1.")
    public int depth_framework = 1;
}
