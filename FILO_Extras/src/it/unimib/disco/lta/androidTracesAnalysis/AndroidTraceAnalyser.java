package it.unimib.disco.lta.androidTracesAnalysis;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import utilities.Utilities;

public class AndroidTraceAnalyser {
	/* Get the logger for the actual class name to be printed on */
	private static final Logger log = Logger.getLogger(AndroidTraceAnalyser.class);

	private String dmtracedump;
	private int depthFramework, depthApplication;

	public AndroidTraceAnalyser(String dmtracedump, int depthApplication, int depthFramework) {
		this.dmtracedump = dmtracedump;
		this.depthFramework = depthFramework;
		this.depthApplication = depthApplication;
	}


	public void parseDirectory(File tracePathFile) {
		String tracePath = tracePathFile.getAbsolutePath();
		if (tracePath.contains("Threads")) {
			log.warn("Name clash: skipping " + tracePath + " folder.");
			return; // Avoiding analysing the results.
		}
		cleanPreviousRuns(tracePath);

		for (String subFilePath : tracePathFile.list()) {
			File subFile = new File(subFilePath);
			if (subFile.isDirectory()) {
				parseDirectory(subFile);
			}
		}
	}

	private void cleanPreviousRuns(String tracePath) {
		log.info("Cleaining target directory from previous results in folder " + tracePath);
		Utilities.runtimeExec("rm -r " + tracePath + File.separator + "FilteredThreads " + tracePath + File.separator + "Threads");
	}

	public void parseFile(File tracePathFile) {
		log.info(tracePathFile.getPath() + " is going to be analysed.");

		String dumpedTracePath =  dumpTrace(tracePathFile.getAbsolutePath());


		Map<String,List<String>> filteredThreadsTraces = parseDumpedTrace(dumpedTracePath);


		List<String> methods = getMethodsList(filteredThreadsTraces);
		String json = methodsToJson(methods);
		String jsonFileName = tracePathFile.getAbsolutePath().substring(0, dumpedTracePath.lastIndexOf('.'));
		if (jsonFileName.substring(0,jsonFileName.lastIndexOf(File.separator)).contains("_")){
			jsonFileName = jsonFileName.substring(0,jsonFileName.lastIndexOf('_'));
		}
		jsonFileName = jsonFileName + ".json";
		Utilities.writeFile(json, jsonFileName);
		log.info(jsonFileName + " created. " + methods.size() + " methods found"); 
	}


	private String dumpTrace(String tracePath) {
		log.info("dumping " + tracePath);
		String command = dmtracedump + " -o " + tracePath ;
		String outputFileName = tracePath.substring(0, tracePath.lastIndexOf('.')) + ".txt";
		Utilities.runtimeExec(command, outputFileName);
		log.info("File " + outputFileName + " created.");
		return outputFileName;
	}

	private Map<String,List<String>> parseDumpedTrace(String dumpedTracePath) {
		log.info("Analysing dumped trace.");
		Map<String,List<String>> threadsTraces = getThreadsTraces(dumpedTracePath);
		String folderPath = dumpedTracePath.substring(0, dumpedTracePath.lastIndexOf(File.separator)+1) + "Threads";
		new File(folderPath).mkdirs();

		for (String threadName : threadsTraces.keySet()) {
			Utilities.writeFile(threadsTraces.get(threadName), folderPath + File.separator + threadName + ".txt");	
		}

		String filteredFolderPath = dumpedTracePath.substring(0, dumpedTracePath.lastIndexOf(File.separator)+1) + "FilteredThreads";
		new File(filteredFolderPath).mkdirs();
		Map<String,List<String>> filteredTraces = filterTraces(threadsTraces);
		for (String threadName : filteredTraces.keySet()) {
			Utilities.writeFile(filteredTraces.get(threadName), filteredFolderPath + File.separator + threadName + ".txt");	
		}
		return filteredTraces;
	}

	private Map<String, List<String>> getThreadsTraces(String tracePath){
		Map<String, List<String>> threadsTraces = new HashMap<>();
		Map<String, String> threadsNames = new HashMap<>();
		Scanner scanner;
		String line = "";
		try {
			scanner = new Scanner(new File(tracePath));
			//			while (scanner.hasNextLine()) {
			line = scanner.nextLine();
			while (!line.contains("Threads")) {
				line = scanner.nextLine();
			}
			line = scanner.nextLine();
			while (!line.contains("Trace")){
				if (line.charAt(0) == ' '){
					line = line.substring(1); // remove first white character for old tracer versions
				}
				int i = line.indexOf(" ");
				String tmp1 = line.substring(0,i);
				String tmp2 = line.substring(i+1);
				threadsNames.put(tmp1,tmp2);
				line = scanner.nextLine();
			}

			do{
				line = scanner.nextLine();
				if (line.contains("???.???")){  // Sometimes this appears in old Traces versions
					continue;
				}
				if (line.charAt(0) == ' '){
					line = line.substring(1); // remove first white character for old tracer versions
				}
				String threadID = line.substring(0,line.indexOf(" "));
				int index = line.indexOf(" ") +1;

				String action = line.substring(index, line.substring(index).indexOf(" ") + index);
				index += action.length();
				//				while (!Character.isLetter(line.charAt(index))) {
				//					index++;
				//				}
				while(Character.isWhitespace(line.charAt(index)) || Character.isDigit(line.charAt(index))){
					index++;
				}

				String method = line.substring(index,line.lastIndexOf(')')+1);
				String threadName = threadsNames.get(threadID);
				if (!threadsTraces.containsKey(threadName)){
					threadsTraces.put(threadName,new ArrayList<>());
				}
				threadsTraces.get(threadName).add(action + " " + method);
			}while(scanner.hasNextLine());
		} catch (Exception e) {
			System.err.println(line);
			e.printStackTrace();
		}
		return threadsTraces;
	}

	public static final String ENT = "ent", XIT = "xit";

	public Map<String, List<String>> filterTraces(Map<String,List<String>> threadsTraces) {
		Map<String,List<String>> filteredTraces = new HashMap<>();
		Stack<String> toKeep = new Stack<>();

		for (String threadName : threadsTraces.keySet()) {
			List<String> trace = threadsTraces.get(threadName);
			//			String caller = "";
			filteredTraces.put(threadName, new ArrayList<>());
			for (String traceLine : trace) {
				String[] split = traceLine.split(" ");
				String action = split[0];
				String methodName = split[1];

				if (Utilities.isToIgnore(methodName)){
					continue;
				}
				if (toKeep.isEmpty()){ 
					if (action.equals(XIT)){ // We do not have the ENT in the trace, we skip the invocation.
						continue;
					}

					if (Utilities.isFrameworkMethod(methodName)){ // We skip top level Framework methods, as they are likely invoked by the framework.
						continue;
					}

					filteredTraces.get(threadName).add(traceLine);
					toKeep.push(methodName);				
				} else{
					if (action.equals(XIT)){
						if (toKeep.peek().equals(methodName)){
							filteredTraces.get(threadName).add(traceLine);
							toKeep.pop();
						}
					} else{ // ENT case
						if (_isBoundaryMethod(methodName, toKeep)){
						//if (_oldisBoundaryMethod(methodName, toKeep)){
							filteredTraces.get(threadName).add(traceLine);
							toKeep.push(methodName);
						}
					}
				}
			}
		}

		return filteredTraces;

	}

	private boolean _isBoundaryMethod(String methodName, Stack<String> toKeep) {		
		@SuppressWarnings("unchecked")
		Stack<String> toKeepClone = (Stack<String>) toKeep.clone();
		for (int i = 1; i <= Math.max(depthFramework, depthApplication); i++) {
			if (i > 1)
				System.out.print("");

			boolean methodIsFramework = Utilities.isFrameworkMethod(methodName);

			if (!toKeepClone.isEmpty()) {
				//String method = toKeepClone.pop();
				String parent = "";
				boolean parentIsFramework = false;
				if (!toKeepClone.isEmpty()) {
					parent =toKeepClone.peek();
					parentIsFramework = Utilities.isFrameworkMethod(parent);
				}else {
					parentIsFramework = true;
				}
				if (i <= depthFramework) {
					if (methodIsFramework && !parentIsFramework)
						return true;
				}
				if (i <= depthApplication) {
					if (!methodIsFramework && parentIsFramework)
						return true;
				}
				methodName = toKeepClone.pop();
			} else {
				if (!methodIsFramework)
					return true;
				System.out.println("STACK EMPTY; WHAT NOW?");
			}
		}
		return false;

	}
	

//	private boolean _oldisBoundaryMethod(String methodName, Stack<String> toKeep) {
//		boolean isBoundaryMethod = false;
//		if (Utilities.isFrameworkMethod(methodName) ^ Utilities.isFrameworkMethod(toKeep.peek())) {
//			isBoundaryMethod = true;
//		} else if (boundary_threshold > 1) {
//			@SuppressWarnings("unchecked")
//			Stack<String> toKeepClone = (Stack<String>) toKeep.clone();
//			for (int i = 2; i <= boundary_threshold; i++) {
//				if (!toKeepClone.isEmpty()) {
//					String method = toKeepClone.pop();
//					String parent = "";
//					if (!toKeepClone.isEmpty())
//						parent =toKeepClone.peek();
//					if (parent.equals("") || (Utilities.isFrameworkMethod(method) ^ Utilities.isFrameworkMethod(parent))) {
//						isBoundaryMethod = true;
//					} 
//				} else if (toKeepClone.size() == 1){
//					System.out.println("AAARGH");
//				} else {
//					break;
//				}
//			}
//		}
//
//
//		return isBoundaryMethod;
//	}


	private static List<String> getMethodsList(Map<String, List<String>> filteredThreadsTraces){
		List<String> methodsList = new ArrayList<>();

		for (List<String> threadMethods : filteredThreadsTraces.values()) {
			for (String methodLine : threadMethods) {
				methodsList.add(cleanMethod(methodLine));
			}
		}
		return methodsList;
	}

	private static String cleanMethod(String methodLine) {

		String cleanMethodLine = methodLine.substring(methodLine.indexOf(' '));
		while (!Character.isLetter(cleanMethodLine.charAt(0))){
			cleanMethodLine = cleanMethodLine.substring(1);
		}
		return cleanMethodLine;
	}

	@SuppressWarnings("unchecked")
	private static String methodsToJson(List<String> methods) {
		log.info("Obtaining JSON String from methods list.");
		ConcurrentHashMap<String, JSONObject> classesHashMap = new ConcurrentHashMap<>();

		for (String method : methods) {
			//			if (method.contains("TableManager.getTable")){
			//				System.out.println("FOUND");
			//			}
			method = method.replace('/', '.'); // Replaces the / in . parameters always use / in the trace, in old versions, also the classes use /
			String tmp = method.substring(0,method.indexOf('('));
			String methodName = tmp.substring(tmp.lastIndexOf('.') + 1, tmp.indexOf(' '));
			String className = method.substring(0, tmp.lastIndexOf('.'));
			if (className.length() < 4){
				System.out.println("Skipping " + className + " for short name");
				continue;
			}
			String parameters = method.substring(method.indexOf('(')+1, method.indexOf(')'));
			//			String dotParameters = parameters.replace('/', '.');
			List<String> parametersList = extractJNIParameters(parameters);
			JSONArray parametersJSONArray = new JSONArray();

			for (String parameter : parametersList) {
				parametersJSONArray.add(parameter);
			}
			if (!classesHashMap.containsKey(className)){
				classesHashMap.put(className, new JSONObject());
				classesHashMap.get(className).put("className", className);
				classesHashMap.get(className).put("methods", new JSONArray());
			}
			JSONObject methodJSONObj = new JSONObject();
			methodJSONObj.put("methodName", methodName);
			methodJSONObj.put("parametersTypes", parametersJSONArray);
			JSONArray methodsJSONArray = (JSONArray)classesHashMap.get(className).get("methods");
			if (!methodsJSONArray.contains(methodJSONObj)){
				methodsJSONArray.add(methodJSONObj);
			}
		}

		JSONArray classesArray = new JSONArray();
		classesArray.addAll(classesHashMap.values());
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("classes", classesArray);

		return jsonObject.toJSONString();
	}

	private static ArrayList<String> extractJNIParameters(String parameters) {
		ArrayList<String> parametersList = new ArrayList<>();

		if (parameters.length() == 0){
			return parametersList;
		}

		String newparameter = "";
		int end = 0;
		switch (parameters.charAt(0)) {
		case 'Z':
			newparameter = "boolean";
			break;
		case 'B':
			newparameter = "byte";
			break;
		case 'C':
			newparameter = "char";
			break;
		case 'S':
			newparameter = "short";
			break;
		case 'I':
			newparameter = "int";
			break;
		case 'J':
			newparameter = "long";
			break;
		case 'F':
			newparameter = "float";
			break;
		case 'D':
			newparameter = "double";
			break;
		case 'L':
			end = parameters.indexOf(';');
			newparameter = parameters.substring(1, end);
			break;
		case '[':
			switch (parameters.charAt(1)) {
			case '[':
				System.out.println("MATRIX FOUND");
				System.out.println(parameters);
				if (parameters.charAt(2) == 'L') {
					end = parameters.indexOf(';');
					newparameter = parameters.substring(0, end + 1);
				} else {
					newparameter = parameters.substring(0, 3);
					end = end + 2;
				}
				break;
			case 'L':
				end = parameters.indexOf(';');
				newparameter = parameters.substring(0, end + 1);
				break;
			default:
				newparameter = parameters.substring(0, 2);
				end++;
				break;
			}
			break;
		default:
			System.out.println("UNABLE TO PARSE PARAMETERS " + parameters);
			newparameter = "######";
			break;
		}
		parametersList.add(newparameter);
		parameters = parameters.substring(end + 1);
		List<String> nextParams = extractJNIParameters(parameters);
		parametersList.addAll(nextParams);
		return parametersList;
	}
}




